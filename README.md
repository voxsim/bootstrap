
MAGIC BOOTSTRAP AND METRICS ANALYSIS
====================================

The scripts running the algorithms are located under scripts/

In order to process datasets with the script, parameters at the top
of each scripts must be verified and modified based on your own data
directories architecture. However, there is a certain ways of arranging
the directory structure to be able to run the bootstrap (see the script
for more details). The metrics analysis script is built to work directly
with the output structure of the bootstrapping script as its input. Some
parameters have to be modified there too though.

Magic Bootstrap
---------------

Script : magic_bootstrap.py

Magic Metrics
-------------

Script : extract_metrics_NEX.py
