from toolbox.utils import verbose_wrapper, parser as param_parser

class fb_handle:

    executable = "/home/local/USHERBROOKE/vala2004/dev/MITK/MITK-build/bin/MitkFiberfox"

    @verbose_wrapper('')
    def get_run_command(self, input, param, output):
        return self.executable + " -i " + input + " -o " + output + " -p " + param

    def get_executable(self):
        return self.executable

    def read_params(self, path):
        return param_parser(path)