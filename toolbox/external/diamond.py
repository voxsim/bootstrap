from toolbox.utils import verbose_wrapper


class md_handle:

    def __init__(self, executable_path, nb_threads=8):
        self.executable = "{0} -n 3 -p {1} --fascicle diamondNCcyl".format(executable_path, nb_threads)

    def append_argument(self, argument):
        self.executable += " {0}".format(argument)

    def automose(self):
        self.executable += " --automose aicu"

    @verbose_wrapper('1')
    def get_run_command(self, input, output):
        return self.executable + " -i " + input + " -o " + output

    def get_executable(self):
        return self.executable
