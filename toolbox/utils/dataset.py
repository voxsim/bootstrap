import nibabel as nib
from shutil import copyfile

class dataset:

    _affine = None
    _shape = None
    _data = None
    _header = None
    _bvals = None
    _bvecs = None

    def __init__(self, nib_file_handle = None, bvals_file=None, bvecs_file=None):
        if nib_file_handle is not None:
            self._affine = nib_file_handle.affine
            self._shape = nib_file_handle.shape
            self._data = nib_file_handle.get_fdata()
            self._header = nib_file_handle.get_header()
        if bvals_file is not None:
            with(open(bvals_file)):
                self._bvals = bvals_file
        if bvecs_file is not None:
            with(open(bvecs_file)):
                self._bvecs = bvecs_file

    @property
    def affine(self):
        return self._affine

    @property
    def shape(self):
        return self._shape

    @property
    def header(self):
        return self._header

    @property
    def bvals(self):
        return self._bvals

    @property
    def bvecs(self):
        return self._bvecs

    @property
    def data(self):
        return self._data

    @affine.setter
    def affine(self, affine):
        self._affine = affine

    @shape.setter
    def shape(self, shape):
        self._shape = shape

    @header.setter
    def header(self, header):
        self._header = header

    @bvals.setter
    def bvals(self, bvals):
        self._bvals = bvals

    @bvecs.setter
    def bvecs(self, bvecs):
        self._bvecs = bvecs

    @data.setter
    def data(self, data, shape=None):
        self._data = data
        if shape is not None:
            self._shape = shape

    def save(self, path, save_bvecs=True, save_bvals=True):
        img = nib.Nifti1Image(self.data, self.affine)
        nib.save(img, path)
        if save_bvals:
            copyfile(self.bvals, "{0}.bvals".format(path))
        if save_bvecs:
            copyfile(self.bvecs, "{0}.bvecs".format(path))

    def print_self(self):
        print(self.bvals)
        print(self.bvecs)
        print(self.data)
        print(self.shape)
