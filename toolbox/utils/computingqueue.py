

class ComputingQueue:

    def __init__(self, file_path):
        self._file_path = file_path
        self._file = open(self._file_path, "a")

    def queue_process(self, process, *args):
        self._file.write(args[0] + "\n")
        self._file.flush()

    def join(self):
        self._file.close()

    def __getstate__(self):
        state = self.__dict__.copy()
        state["_file"] = None

        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self._file = open(self._file_path, "a")
