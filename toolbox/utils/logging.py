import sys


class LogFile:

    def __init__(self, log_file_name, pipe=sys.stdout):
        self._log_file_name = log_file_name
        self._log_file = open(log_file_name, "a+")
        self._pipe = pipe
        self._tag = ""

    def __enter__(self):
        return self

    def set_log_tag(self, tag):
        self._tag = tag

    def write(self, string, to_pipe=True):
        self._log_file.write(" ".join([self._tag, string]))
        self._log_file.flush()

        if to_pipe:
            self._pipe.write(" ".join([self._tag, string]))
            self._pipe.flush()

    def log_command(self, command, exec_format, args_format, to_pipe=True):
        str_out = ""
        str_tmp = ""

        command_out = command.split(' ')

        str_out += (exec_format.format(command_out[0]))

        for co in command_out[1:]:
            if co[0] == '-':
                str_tmp += co + " "
            else:
                str_out += (args_format.format(str_tmp + co))
                str_tmp = ""

        self.write(str_out, to_pipe)

    def close(self):
        self.write("Closing logging file")
        self._log_file.close()

    # def __exit__(self, tp, value, tb):
    #     self.close()

    def __getstate__(self):
        print("pickling self logging")
        state = self.__dict__.copy()
        state['_log_file'] = None
        return state

    def __setstate__(self, state):
        print("openning self pickled state")
        self.__dict__.update(state)
        self._log_file = open(self._log_file_name, "a+")

    def __del__(self):
        self._log_file = None
