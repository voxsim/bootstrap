from multiprocess import Pool, Lock
import os
import tarfile
from os import path
from shutil import rmtree
from subprocess import call

env = dict(os.environ)
env['LC_ALL'] = 'en_US.UTF-8'

log_lock = Lock()


def plog(res):
    log_lock.acquire()
    print("The end of me {}".format(res))
    log_lock.release()

class process_queuer:

    def __init__(self, nb_processes):
        self._pool = Pool(processes=nb_processes)



    def queue_process(self, process, *args):
        try:
            print("Submitting to pool : {}".format(process))
            print("args : {}".format(args))
            return self._pool.apply_async(process, args=tuple(args))
        except Exception as e:
            print("There was a problem")
            print(e.message)

    def join(self):
        print("Closing pool")
        self._pool.close()
        self._pool.join()




def execute_cli(command, acquisition_path, tar_path, tar_name, log_file):
    print("=> Starting diamond")
    print("=> Command {0}".format(command))
    call(command, shell=True, stdout=open(log_file, 'w+'), env=env)
    tf = tarfile.open(path.join(tar_path, tar_name))
    tf.add(acquisition_path, arcname=acquisition_path.split("/")[-1])
    tf.close()
    rmtree(acquisition_path)