import xml.etree.ElementTree as ET
from xml.etree.ElementTree import ElementTree as tree


class parser:

    def __init__(self, path, root=None, position=None):
        self._path = path
        if root is not None:
            self._root = root
        else:
            self._root = ET.parse(path).getroot()

        if position is not None:
            self._position = position
        else:
            self.go_root()

    def _find_node(self, *args):
        return self._position.find("".join(str(e) + "/" for e in args).strip("/"))

    def set_attribute(self, attribute, value):
        self._position.attrib[attribute] = str(value)
        return self

    def go_root(self):
        self._position = self._root

    def go_to(self, *args):
        position = self._find_node(*args)
        self._position = position if position else self._position
        return self.get_current()

    def find_node(self, *args):
        return parser(self._path, self._root, self._find_node(*args))

    def get_childs(self, tag):
        return [parser(self._path, self._root, child) for child in self._position.iter() if child.tag == tag]

    def get_current(self):
        return self._position

    def save(self, path, name):
        tree(self._root).write("{0}/{1}".format(path, name))
