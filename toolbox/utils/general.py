def flatten(a):
    return [x for y in a for x in y] if isinstance(a, (list,)) and isinstance(a[0], (list,)) else a


def scramble_parameters(cp_params):
    cp_pairs = {}

    for cp_key, cp in cp_params.iteritems():
        cp_pairs[cp_key] = []

        item_lists = []

        keys = cp.keys()
        for cp_it_key, cp_item in cp.iteritems():
            item_lists.append(cp_item)

        for i in range(1, len(item_lists)):
            item_lists[0] = flatten(
                map(lambda x, y: map(lambda v: flatten([x]) + [v], item_lists[i]), item_lists[0], item_lists[i])
            )

        cp_pairs[cp_key] = [{keys[i]: item[i] for i in range(len(item))} for item in item_lists[0] if None not in item]

    return cp_pairs


import nibabel as nib
import numpy as np
from os import path


def concatenate_data(file_list, file_type, output_directory, concatenated_root='concatenated'):
    if file_type == ".nii.gz":
        image_list = [nib.load(x) for x in file_list]
        data_tuple = tuple([x.get_data() for x in image_list])
        concatenated_data = np.concatenate(data_tuple, axis=3)
        nib.save(nib.Nifti1Image(concatenated_data,
                                 image_list[0].affine
                                 ),
                 output_directory + concatenated_root + '.nii.gz'
                 )
    elif file_type == ".bvec":
        data_tuple = tuple([np.loadtxt(x) for x in file_list])
        concatenated_data = np.concatenate(data_tuple, axis=1)
        np.savetxt(output_directory + concatenated_root + file_type, concatenated_data, "%.8f")
    elif file_type == ".bval":
        data_tuple = tuple([np.loadtxt(x) for x in file_list])
        concatenated_data = np.concatenate(data_tuple)
        concatenated_data.shape = (1, len(concatenated_data))
        np.savetxt(output_directory + concatenated_root + file_type, concatenated_data, "%.8f")


def concatenated_data_full(input_list, output_directory, output_name, bvecs=True):
    concatenate_data(["{0}.nii.gz".format(i) for i in input_list], ".nii.gz", output_directory, output_name)
    concatenate_data(["{0}.{1}".format(i, "bvecs" if bvecs else "bvec") for i in input_list],
                     ".bvec",
                     output_directory,
                     output_name
                     )
    concatenate_data(["{0}.{1}".format(i, "bvals" if bvecs else "bval") for i in input_list],
                     ".bval",
                     output_directory,
                     output_name
                     )


def concatenate_md_data(base_root, linear_root, spherical_root, name_base):
    concatenated_data_full(
        ["{0}/{1}".format(linear_root, name_base), "{0}/{1}".format(spherical_root, name_base)],
        base_root,
        name_base + "_concatenated"
    )


def concatenate_multiple_md_data(base_root, dataset_pairings, output_root, output_names, bvecs=True):
    for pairings in map(lambda x, y: [x] + [y], dataset_pairings, output_names):
        concatenated_data_full([path.join(base_root, pairing) for pairing in pairings[0]],
                               output_root,
                               pairings[1],
                               bvecs
                               )


def open_datasets(datasets_list):
    out_datasets_list = []
