from scipy.special import i0, i1
from numpy import pi, exp, sqrt


def epsil(theta):
    t2 = theta ** 2.
    return 2. + t2 - pi / 8. * exp(-t2 / 2.) * (((2. + t2) * i0(t2 / 4.) + t2 * i1(t2 / 4.)) ** 2.)


def g(theta, r):
    epsilon = epsil(theta)
    val = sqrt(epsilon * (1. + r ** 2.) - 2.)
    return val


def compute_rice_parameters(signal, noise, t0=30., eps=1E-6):
    r = signal.mean() / noise.var()
    print("Signal : {} | {}".format(signal.mean(), signal.std()))
    ti = t0
    g0 = g(t0, r)

    print("Initial values {} | {}".format(ti, g0))

    while (g0 - ti) >= eps:
        ti = g(ti, r)
        g0 = g(g0, r)

    estar = epsil(ti)
    var2 = sqrt(signal.var()) / sqrt(estar)

    return sqrt(signal.mean() ** 2. + (estar - 2.) * var2)
