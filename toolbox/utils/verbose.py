def verbose_wrapper(param):
    def func_wrapper(func):
        def arg_wrapper(*args, **kwargs):
            v = kwargs.pop('verbose')
            return '{0}{1}'.format(func(*args, **kwargs), ' -v {0}'.format(param).rstrip(' ') if v else '')
        return arg_wrapper
    return func_wrapper
