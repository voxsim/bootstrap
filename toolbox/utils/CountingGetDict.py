

class CountingGetDict:

    def __init__(self):
        self._items = dict()

    def __getitem__(self, item):
        try:
            self._items[item] = (self._items[item][0], self._items[item][1] + 1)
            return self._items[item]
        except KeyError:
            raise KeyError("{} is not in dictionary".format(item))

    def __setitem__(self, key, value):
        self._items[key] = (value, 0)

    def keys(self):
        return self._items.keys()