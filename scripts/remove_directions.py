from toolbox.utils import concatenate_multiple_md_data
import numpy as np
import nibabel as nib
from os.path import join

base_root="/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout/d2-15"
linear_root = "param_bdel1"
planar_root = "param_bdel-05"
spherical_root = "param_bdel0"


def remove_directions():
    dataset = join(base_root, spherical_root + "_nonoise", "fiberfox")

    bvals = np.loadtxt(dataset + ".bvals")
    bvecs = np.loadtxt(dataset + ".bvecs")
    data = nib.load(dataset + ".nii.gz")

    np.savetxt(dataset + ".bvals", bvals, "%.8f")
    np.savetxt(dataset + ".bvecs", bvecs, "%.8f")

    d = data.get_data()
    img = nib.Nifti1Image(dataobj=d[..., 3:], affine=data.affine)
    nib.save(img, dataset + ".nii.gz")


if __name__ == "__main__":
    remove_directions()
