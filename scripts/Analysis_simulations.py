import os
from math import *
import numpy as np
import numpy.linalg as linalg
import nibabel as nib
import nrrd #from pynrrd
import itertools
from tqdm import tqdm
import scipy.stats

import matplotlib
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
import matplotlib.pyplot as plt
import pylab

# import time
# import datetime
# start_time = datetime.datetime.now()
# print datetime.datetime.now() - start_time

# WORKING_DIR = os.getcwd()
WORKING_DIR = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo"


data_name = WORKING_DIR + "/new_run"
diamond_directory = data_name + "/diamondout/"
fiberfox_directory = data_name + "/fiberfoxout/"

Diso_list = [["30",1,"3"]] # , ["15",1.5,"1.5"], ["20",2,"2"], ["25",2.5,"2.5"]]
comparison_directory_list = [WORKING_DIR + "/Comparison_" + x + "/" for x in [y[2] for y in Diso_list]]

data_type_list = ["linlin", "linpla", "linsph"]
snr_string_list = ["nonoise"] # ["snr50", "snr5"]
snr_label_list = [r"$\mathrm{SNR}=\infty$"] # , r"$\mathrm{SNR}=5$"]
color_snr_list = ["Blue", "Red"]
# snr_string_list = ["nonoise", "snr50", "snr5"]
# snr_label_list = [r"$\mathrm{SNR}\to\infty$", r"$\mathrm{SNR}=50$", r"$\mathrm{SNR}=5$"]
# color_snr_list = ["Red", "Green", "Blue"]
# snr_string_list = ["nonoise", "snr50", "snr20", "snr5"]
# snr_label_list = [r"$\mathrm{SNR}\to\infty$", r"$\mathrm{SNR}=50$", r"$\mathrm{SNR}=20$", r"$\mathrm{SNR}=5$"]
# color_snr_list = ["Red", "Green", "Blue", "DarkViolet"]

nb_fascicles = 3

# Fiberfox parameters
D_para_fiber = 0.0017 # in mm2/s
D_perp_fiber = 0.0004 # in mm2/s
mean_D = 1./3*(D_para_fiber + 2*D_perp_fiber)
FA_fiber = 3./sqrt(2)*sqrt(1./3*((D_para_fiber-mean_D)*(D_para_fiber-mean_D) +2*(D_perp_fiber-mean_D)*(D_perp_fiber-mean_D))/(D_para_fiber*D_para_fiber + 2*D_perp_fiber*D_perp_fiber))
D_FW = 3 # in mm2/ms
fiber_orientations = [[1,0,0], [0,1,0], [0,1./np.sqrt(2),1./np.sqrt(2)]]
radius_bundles = [3.5, 3, 4]

def main():
    for it_Diso in range(len(Diso_list)):

        Diso_string_dir = Diso_list[it_Diso][0]
        Diso = Diso_list[it_Diso][1]
        Diso_string = Diso_list[it_Diso][2]
        comparison_directory = comparison_directory_list[it_Diso]

        check_dir(comparison_directory)

        reference_affine = diamond_directory + "d2-" + Diso_string_dir + "/linlin/dwi_ini_nonoise.nii.gz"
        affine = nib.load(reference_affine).affine

        fiberfox_mosemap = nib.load(fiberfox_directory + "fibers_maps/gt_mosemap.nii").get_data()
        create_Nifti(fiberfox_mosemap, affine, comparison_directory, "fiberfox_mosemap_Diso_" + Diso_string)

        # Fiberfox free water fraction
        fiberfox_fintrafiberwater = np.array(nrrd.read(fiberfox_directory + "d2-" + Diso_string_dir + "/param_bdel1_nonoise/fiberfox_Compartment2.nrrd")[0])
        fiberfox_fFW = np.array(nrrd.read(fiberfox_directory + "d2-" + Diso_string_dir + "/param_bdel1_nonoise/fiberfox_Compartment3.nrrd")[0]) \
                      + np.array(nrrd.read(fiberfox_directory + "d2-" + Diso_string_dir + "/param_bdel1_nonoise/fiberfox_Compartment4.nrrd")[0])
        create_Nifti(fiberfox_fFW, affine, comparison_directory, "fiberfox_fFW_Diso_" + Diso_string)
        create_Nifti(fiberfox_fintrafiberwater, affine, comparison_directory, "fiberfox_fintrafiberwater_Diso_" + Diso_string)
        fiberfox_fFW = nib.load(comparison_directory + "fiberfox_fFW_Diso_" + Diso_string + ".nii.gz").get_data()
        fiberfox_fintrafiberwater = nib.load(comparison_directory + "fiberfox_fintrafiberwater_Diso_" + Diso_string + ".nii.gz").get_data()

        list_over_snr_fAD_bundle1 = [[],[],[],[]]
        list_over_snr_fAD_bundle2 = [[],[],[],[]]
        list_over_snr_fAD_bundle3 = [[],[],[],[]]
        list_over_snr_fRD_bundle1 = [[],[],[],[]]
        list_over_snr_fRD_bundle2 = [[],[],[],[]]
        list_over_snr_fRD_bundle3 = [[],[],[],[]]
        list_over_snr_fFA_bundle1 = [[],[],[],[]]
        list_over_snr_fFA_bundle2 = [[],[],[],[]]
        list_over_snr_fFA_bundle3 = [[],[],[],[]]
        list_over_snr_deviation_bundle1 = [[],[],[],[]]
        list_over_snr_deviation_bundle2 = [[],[],[],[]]
        list_over_snr_deviation_bundle3 = [[],[],[],[]]
        list_over_snr_fFW = [[],[],[],[]]

        for it_snr, snr_string in enumerate(snr_string_list):
            # DIAMOND model selection maps
            mosemap_data = nrrd.read(fiberfox_directory + "concatenated/d2-" + Diso_string_dir + "/mosemaps/" + snr_string + "_mosemap.nrrd")[0]
            create_Nifti(mosemap_data, affine, comparison_directory, "diamond_mosemap_Diso_" + Diso_string + "_" + snr_string)

            nb_voxel_x, nb_voxel_y, nb_voxel_z = mosemap_data.shape
            one_fiber_mask = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z))
            two_fiber_mask = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z))
            three_fiber_mask = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z))
            mismatch_mosemap_mask = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z))
            for it_x in range(nb_voxel_x):
                for it_y in range(nb_voxel_y):
                    for it_z in range(nb_voxel_z):
                        if mosemap_data[it_x][it_y][it_z] != fiberfox_mosemap[it_x][it_y][it_z]:
                            mismatch_mosemap_mask[it_x][it_y][it_z] = 1
                        if mosemap_data[it_x][it_y][it_z] == 1:
                            one_fiber_mask[it_x][it_y][it_z] = 1
                        elif mosemap_data[it_x][it_y][it_z] == 2:
                            two_fiber_mask[it_x][it_y][it_z] = 1
                        elif mosemap_data[it_x][it_y][it_z] == 3:
                            three_fiber_mask[it_x][it_y][it_z] = 1
            create_Nifti(one_fiber_mask, affine, comparison_directory, "mask_diamond_Diso_" + Diso_string + "_one_fiber_" + snr_string)
            create_Nifti(two_fiber_mask, affine, comparison_directory, "mask_diamond_Diso_" + Diso_string + "_two_fiber_" + snr_string)
            create_Nifti(three_fiber_mask, affine, comparison_directory, "mask_diamond_Diso_" + Diso_string + "_three_fiber_" + snr_string)
            create_Nifti(mismatch_mosemap_mask, affine, comparison_directory, "mask_mismatch_mosemap_fiberfox_diamond_Diso_" + Diso_string + "_" + snr_string)

            for encoding in data_type_list:
                #fiberfox_signal = nib.load(fiberfox_directory + "concatenated/" + snr_string + "_" + encoding + "_nn.nii.gz").get_data()

                # DIAMOND quantities
                diamond_data_dir = diamond_directory + "d2-" + Diso_string_dir + "/" + encoding + "/diamond_" + snr_string + "/"
                if "nonoise" == "nonoise":
                    root = "d_" + snr_string
                else:
                    root = "md_" + snr_string

                fractions = nrrd.read(diamond_data_dir + root + "_fractions.nrrd")[0]
                nb_compartments, nb_voxel_x, nb_voxel_y, nb_voxel_z = fractions.shape
                file_name_fascicles = [diamond_data_dir + root + "_t" + str(i) + ".nrrd" for i in range(nb_fascicles)]
                fractions_fascicles = np.array([fractions[0], fractions[1], fractions[2]]) # These fractions are associated to fascicles with largest to smallest fFA
                diamond_fFW = np.array(fractions[-1])
                list_over_snr_fFW[it_snr].append([float(x) for x in np.array(diamond_fFW).flatten()])
                create_Nifti(diamond_fFW, affine, comparison_directory, "diamond_fFW_Diso_" + Diso_string + "_" + encoding + "_" + snr_string) # Free water fraction
                data_fascicles = [nrrd.read(file_name_fascicles[nf])[0] for nf in range(nb_fascicles)]

                difference_fFW = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                maximum = 0
                for it_x in range(nb_voxel_x):
                    for it_y in range(nb_voxel_y):
                        for it_z in range(nb_voxel_z):
                            difference_fFW[it_x][it_y][it_z] = diamond_fFW[it_x][it_y][it_z] - fiberfox_fFW[it_x][it_y][it_z]
                            if np.abs(difference_fFW[it_x][it_y][it_z]) > maximum:
                                maximum = np.abs(difference_fFW[it_x][it_y][it_z])
                create_Nifti(difference_fFW, affine, comparison_directory, "comp_fFW_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)

                color_mapped_difference_fFW = rgb_map_normalized_difference(1./maximum*np.array(difference_fFW))
                tuple_color_voxel = (nb_voxel_x, nb_voxel_y, nb_voxel_z, 3)
                reshape_and_Nifti(color_mapped_difference_fFW, tuple_color_voxel, affine, comparison_directory, "comp_fFW_color_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)

                list_fAD_bundle1 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fAD_bundle2 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fAD_bundle3 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fRD_bundle1 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fRD_bundle2 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fRD_bundle3 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fFA_bundle1 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fFA_bundle2 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_fFA_bundle3 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_orientations_bundle1 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z, 3*nb_fascicles), dtype=np.float32)
                list_orientations_bundle2 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z, 3*nb_fascicles), dtype=np.float32)
                list_orientations_bundle3 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z, 3*nb_fascicles), dtype=np.float32)
                list_deviation_orientations_bundle1 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_deviation_orientations_bundle2 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_deviation_orientations_bundle3 = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)
                list_orientations = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z, 3*nb_fascicles), dtype=np.float32)
                list_deviation_orientations = np.zeros((nb_voxel_x, nb_voxel_y, nb_voxel_z), dtype=np.float32)

                for iter in tqdm(itertools.product(range(nb_voxel_x), range(nb_voxel_y), range(nb_voxel_z)), total=nb_voxel_x*nb_voxel_y*nb_voxel_z):
                    it_x = iter[0]
                    it_y = iter[1]
                    it_z = iter[2]

                    if np.sum([fractions[i][it_x][it_y][it_z] for i in range(nb_compartments)]) == 0:
                        continue

                    list_fAD = []
                    list_fRD = []
                    list_fFA = []
                    main_eigenvectors_list = []
                    fractions_fascicles_voxel = []

                    if mosemap_data[it_x][it_y][it_z] == 0:
                        main_eigenvectors_list = [np.zeros(3),np.zeros(3),np.zeros(3)]
                        fractions_fascicles_voxel = [0,0,0]
                        list_fAD = [0,0,0]
                        list_fRD = [0,0,0]
                        list_fFA = [0,0,0]

                    if mosemap_data[it_x][it_y][it_z] == 1:
                        nf = 0
                        data = data_fascicles[nf]
                        fractions_fascicles_voxel.append(fractions_fascicles[nf][it_x][it_y][it_z])
                        matrix_element = [data[n][it_x][it_y][it_z] for n in range(6)]
                        matrix = create_matrix(matrix_element)
                        # The eigenValues[i] are in ascending order, eigenVectors[:,i] are sorted accordingly
                        eigenValues, eigenVectors = linalg.eigh(matrix)
                        main_eigenvectors_list.append(eigenVectors[:, -1])
                        list_fAD.append(eigenValues[-1])
                        list_fRD.append((eigenValues[0] + eigenValues[1])/2.)
                        list_fFA.append(FA(eigenValues))
                        main_eigenvectors_list.append(np.zeros(3))
                        main_eigenvectors_list.append(np.zeros(3))
                        fractions_fascicles_voxel.append(0)
                        fractions_fascicles_voxel.append(0)
                        list_fAD.append(0)
                        list_fAD.append(0)
                        list_fRD.append(0)
                        list_fRD.append(0)
                        list_fFA.append(0)
                        list_fFA.append(0)

                        orientations_list = map_orientations(np.ones(3), main_eigenvectors_list, nb_fascicles)
                        for it in range(3*nb_fascicles):
                            list_orientations[it_x][it_y][it_z][it] = orientations_list[it]

                        diam_fiber_spherical = cartesian_to_spherical_direction(main_eigenvectors_list[0])
                        deviation_list = [angular_difference(diam_fiber_spherical[1],cartesian_to_spherical_direction(x)[1],diam_fiber_spherical[2],cartesian_to_spherical_direction(x)[2]) for x in fiber_orientations]
                        idx_scalar_product = indices_sort(deviation_list)
                        #idx_scalar_product = indices_sort([np.abs(np.dot(main_eigenvectors_list[0], x)) for x in fiber_orientations])
                        if idx_scalar_product[-1] == 0:
                            list_fAD_bundle1[it_x][it_y][it_z] = list_fAD[0]
                            list_fRD_bundle1[it_x][it_y][it_z] = list_fRD[0]
                            list_fFA_bundle1[it_x][it_y][it_z] = list_fFA[0]
                            list_deviation_orientations[it_x][it_y][it_z] = deviation_list[0]
                            list_deviation_orientations_bundle1[it_x][it_y][it_z] = deviation_list[0]
                            for it in range(3 * nb_fascicles):
                                list_orientations_bundle1[it_x][it_y][it_z][it] = orientations_list[it]
                        elif idx_scalar_product[-1] == 1:
                            list_fAD_bundle2[it_x][it_y][it_z] = list_fAD[0]
                            list_fRD_bundle2[it_x][it_y][it_z] = list_fRD[0]
                            list_fFA_bundle2[it_x][it_y][it_z] = list_fFA[0]
                            list_deviation_orientations[it_x][it_y][it_z] = deviation_list[1]
                            list_deviation_orientations_bundle2[it_x][it_y][it_z] = deviation_list[1]
                            for it in range(3 * nb_fascicles):
                                list_orientations_bundle2[it_x][it_y][it_z][it] = orientations_list[it]
                        elif idx_scalar_product[-1] == 2:
                            list_fAD_bundle3[it_x][it_y][it_z] = list_fAD[0]
                            list_fRD_bundle3[it_x][it_y][it_z] = list_fRD[0]
                            list_fFA_bundle3[it_x][it_y][it_z] = list_fFA[0]
                            list_deviation_orientations[it_x][it_y][it_z] = deviation_list[2]
                            list_deviation_orientations_bundle3[it_x][it_y][it_z] = deviation_list[2]
                            for it in range(3 * nb_fascicles):
                                list_orientations_bundle3[it_x][it_y][it_z][it] = orientations_list[it]

                    if mosemap_data[it_x][it_y][it_z] == 2:
                        for nf in range(2):
                            data = data_fascicles[nf]
                            fractions_fascicles_voxel.append(fractions_fascicles[nf][it_x][it_y][it_z])
                            matrix_element = [data[n][it_x][it_y][it_z] for n in range(6)]
                            matrix = create_matrix(matrix_element)
                            # The eigenValues[i] are in ascending order, eigenVectors[:,i] are sorted accordingly
                            eigenValues, eigenVectors = linalg.eigh(matrix)
                            main_eigenvectors_list.append(eigenVectors[:, -1])
                            list_fAD.append(eigenValues[-1])
                            list_fRD.append((eigenValues[0] + eigenValues[1]) / 2.)
                            list_fFA.append(FA(eigenValues))
                        main_eigenvectors_list.append(np.zeros(3))
                        fractions_fascicles_voxel.append(0)
                        list_fAD.append(0)
                        list_fRD.append(0)
                        list_fFA.append(0)

                        orientations_list = map_orientations(np.ones(3), main_eigenvectors_list, nb_fascicles)
                        for it in range(3*nb_fascicles):
                            list_orientations[it_x][it_y][it_z][it] = orientations_list[it]

                        for nf in range(2):
                            diam_fiber_spherical = cartesian_to_spherical_direction(main_eigenvectors_list[nf])
                            deviation_list = [angular_difference(diam_fiber_spherical[1], cartesian_to_spherical_direction(x)[1], diam_fiber_spherical[2], cartesian_to_spherical_direction(x)[2]) for x in fiber_orientations]
                            idx_scalar_product = indices_sort(deviation_list)
                            #idx_scalar_product = indices_sort([np.abs(np.dot(main_eigenvectors_list[nf], x)) for x in fiber_orientations])
                            if idx_scalar_product[-1] == 0:
                                list_fAD_bundle1[it_x][it_y][it_z] = list_fAD[nf]
                                list_fRD_bundle1[it_x][it_y][it_z] = list_fRD[nf]
                                list_fFA_bundle1[it_x][it_y][it_z] = list_fFA[nf]
                                list_deviation_orientations[it_x][it_y][it_z] += deviation_list[0]
                                list_deviation_orientations_bundle1[it_x][it_y][it_z] = deviation_list[0]
                                orientations_list = map_orientations(np.ones(3), [main_eigenvectors_list[nf],np.zeros(3),np.zeros(3)], nb_fascicles)
                                for it in range(3 * nb_fascicles):
                                    list_orientations_bundle1[it_x][it_y][it_z][it] = orientations_list[it]
                            elif idx_scalar_product[-1] == 1:
                                list_fAD_bundle2[it_x][it_y][it_z] = list_fAD[nf]
                                list_fRD_bundle2[it_x][it_y][it_z] = list_fRD[nf]
                                list_fFA_bundle2[it_x][it_y][it_z] = list_fFA[nf]
                                list_deviation_orientations[it_x][it_y][it_z] += deviation_list[1]
                                list_deviation_orientations_bundle2[it_x][it_y][it_z] = deviation_list[1]
                                orientations_list = map_orientations(np.ones(3), [main_eigenvectors_list[nf], np.zeros(3), np.zeros(3)], nb_fascicles)
                                for it in range(3 * nb_fascicles):
                                    list_orientations_bundle2[it_x][it_y][it_z][it] = orientations_list[it]
                            elif idx_scalar_product[-1] == 2:
                                list_fAD_bundle3[it_x][it_y][it_z] = list_fAD[nf]
                                list_fRD_bundle3[it_x][it_y][it_z] = list_fRD[nf]
                                list_fFA_bundle3[it_x][it_y][it_z] = list_fFA[nf]
                                list_deviation_orientations[it_x][it_y][it_z] += deviation_list[2]
                                list_deviation_orientations_bundle3[it_x][it_y][it_z] = deviation_list[2]
                                orientations_list = map_orientations(np.ones(3), [main_eigenvectors_list[nf], np.zeros(3), np.zeros(3)], nb_fascicles)
                                for it in range(3 * nb_fascicles):
                                    list_orientations_bundle3[it_x][it_y][it_z][it] = orientations_list[it]



                    if mosemap_data[it_x][it_y][it_z] == 3:
                        for nf in range(3):
                            data = data_fascicles[nf]
                            fractions_fascicles_voxel.append(fractions_fascicles[nf][it_x][it_y][it_z])
                            matrix_element = [data[n][it_x][it_y][it_z] for n in range(6)]
                            matrix = create_matrix(matrix_element)
                            # The eigenValues[i] are in ascending order, eigenVectors[:,i] are sorted accordingly
                            eigenValues, eigenVectors = linalg.eigh(matrix)
                            main_eigenvectors_list.append(eigenVectors[:, -1])
                            list_fAD.append(eigenValues[-1])
                            list_fRD.append((eigenValues[0] + eigenValues[1]) / 2.)
                            list_fFA.append(FA(eigenValues))

                        orientations_list = map_orientations(np.ones(3), main_eigenvectors_list, nb_fascicles)
                        for it in range(3*nb_fascicles):
                            list_orientations[it_x][it_y][it_z][it] = orientations_list[it]

                        for nf in range(3):
                            diam_fiber_spherical = cartesian_to_spherical_direction(main_eigenvectors_list[nf])
                            deviation_list = [angular_difference(diam_fiber_spherical[1], cartesian_to_spherical_direction(x)[1], diam_fiber_spherical[2], cartesian_to_spherical_direction(x)[2]) for x in fiber_orientations]
                            idx_scalar_product = indices_sort(deviation_list)
                            #idx_scalar_product = indices_sort([np.abs(np.dot(main_eigenvectors_list[nf], x)) for x in fiber_orientations])
                            if idx_scalar_product[-1] == 0:
                                list_fAD_bundle1[it_x][it_y][it_z] = list_fAD[nf]
                                list_fRD_bundle1[it_x][it_y][it_z] = list_fRD[nf]
                                list_fFA_bundle1[it_x][it_y][it_z] = list_fFA[nf]
                                list_deviation_orientations[it_x][it_y][it_z] += deviation_list[0]
                                list_deviation_orientations_bundle1[it_x][it_y][it_z] = deviation_list[0]
                                orientations_list = map_orientations(np.ones(3), [main_eigenvectors_list[nf],np.zeros(3),np.zeros(3)], nb_fascicles)
                                for it in range(3 * nb_fascicles):
                                    list_orientations_bundle1[it_x][it_y][it_z][it] = orientations_list[it]
                            elif idx_scalar_product[-1] == 1:
                                list_fAD_bundle2[it_x][it_y][it_z] = list_fAD[nf]
                                list_fRD_bundle2[it_x][it_y][it_z] = list_fRD[nf]
                                list_fFA_bundle2[it_x][it_y][it_z] = list_fFA[nf]
                                list_deviation_orientations[it_x][it_y][it_z] += deviation_list[1]
                                list_deviation_orientations_bundle2[it_x][it_y][it_z] = deviation_list[1]
                                orientations_list = map_orientations(np.ones(3), [main_eigenvectors_list[nf], np.zeros(3), np.zeros(3)], nb_fascicles)
                                for it in range(3 * nb_fascicles):
                                    list_orientations_bundle2[it_x][it_y][it_z][it] = orientations_list[it]
                            elif idx_scalar_product[-1] == 2:
                                list_fAD_bundle3[it_x][it_y][it_z] = list_fAD[nf]
                                list_fRD_bundle3[it_x][it_y][it_z] = list_fRD[nf]
                                list_fFA_bundle3[it_x][it_y][it_z] = list_fFA[nf]
                                list_deviation_orientations[it_x][it_y][it_z] += deviation_list[2]
                                list_deviation_orientations_bundle3[it_x][it_y][it_z] = deviation_list[2]
                                orientations_list = map_orientations(np.ones(3), [main_eigenvectors_list[nf], np.zeros(3), np.zeros(3)], nb_fascicles)
                                for it in range(3 * nb_fascicles):
                                    list_orientations_bundle3[it_x][it_y][it_z][it] = orientations_list[it]



                # Reshape the lists into arrays of dimension given by a tuple before "Niftification"
                #tuple_voxel = (nb_voxel_x, nb_voxel_y, nb_voxel_z)
                create_Nifti(list_fAD_bundle1, affine, comparison_directory, "diamond_fAD_bundle1_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fAD_bundle2, affine, comparison_directory, "diamond_fAD_bundle2_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fAD_bundle3, affine, comparison_directory, "diamond_fAD_bundle3_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fRD_bundle1, affine, comparison_directory, "diamond_fRD_bundle1_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fRD_bundle2, affine, comparison_directory, "diamond_fRD_bundle2_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fRD_bundle3, affine, comparison_directory, "diamond_fRD_bundle3_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fFA_bundle1, affine, comparison_directory, "diamond_fFA_bundle1_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fFA_bundle2, affine, comparison_directory, "diamond_fFA_bundle2_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_fFA_bundle3, affine, comparison_directory, "diamond_fFA_bundle3_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_deviation_orientations_bundle1, affine, comparison_directory, "diamond_deviation_orientation_bundle1_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_deviation_orientations_bundle2, affine, comparison_directory, "diamond_deviation_orientation_bundle2_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_deviation_orientations_bundle3, affine, comparison_directory, "diamond_deviation_orientation_bundle3_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_deviation_orientations, affine, comparison_directory, "diamond_deviation_orientation_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_orientations_bundle1, affine, comparison_directory, "diamond_orientations_bundle1_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_orientations_bundle2, affine, comparison_directory, "diamond_orientations_bundle2_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_orientations_bundle3, affine, comparison_directory, "diamond_orientations_bundle3_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)
                create_Nifti(list_orientations, affine, comparison_directory, "diamond_local_orientations_Diso_" + Diso_string + "_" + encoding + "_" + snr_string)

                list_over_snr_fAD_bundle1[it_snr].append([x for x in np.array(list_fAD_bundle1).flatten() if x != 0])
                list_over_snr_fAD_bundle2[it_snr].append([x for x in np.array(list_fAD_bundle2).flatten() if x != 0])
                list_over_snr_fAD_bundle3[it_snr].append([x for x in np.array(list_fAD_bundle3).flatten() if x != 0])
                list_over_snr_fRD_bundle1[it_snr].append([x for x in np.array(list_fRD_bundle1).flatten() if x != 0])
                list_over_snr_fRD_bundle2[it_snr].append([x for x in np.array(list_fRD_bundle2).flatten() if x != 0])
                list_over_snr_fRD_bundle3[it_snr].append([x for x in np.array(list_fRD_bundle3).flatten() if x != 0])
                list_over_snr_fFA_bundle1[it_snr].append([x for x in np.array(list_fFA_bundle1).flatten() if x != 0])
                list_over_snr_fFA_bundle2[it_snr].append([x for x in np.array(list_fFA_bundle2).flatten() if x != 0])
                list_over_snr_fFA_bundle3[it_snr].append([x for x in np.array(list_fFA_bundle3).flatten() if x != 0])
                list_over_snr_deviation_bundle1[it_snr].append([x for x in np.array(list_deviation_orientations_bundle1).flatten() if x != 0])
                list_over_snr_deviation_bundle2[it_snr].append([x for x in np.array(list_deviation_orientations_bundle2).flatten() if x != 0])
                list_over_snr_deviation_bundle3[it_snr].append([x for x in np.array(list_deviation_orientations_bundle3).flatten() if x != 0])

        number_bins = 40
        alpha_hist = 0.6
        l_w = 1.25
        tick_size = 16
        title_size = 18
        legend_size = 15

        for it_encoding, encoding in enumerate(data_type_list):
            fig, axes = plt.subplots(nrows=2, ncols=2)
            ax0, ax1, ax2, ax3 = axes.flatten()
            plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=0.3)

            ax0.set_title(r'fAD', fontsize=title_size)
            ax1.set_title(r'fFA', fontsize=title_size)
            ax2.set_title(r'fRD', fontsize=title_size)
            ax3.set_title(r'$\Delta\Theta$', fontsize=title_size)

            for it_snr, snr_string in enumerate(snr_string_list):

                mean_fAD = np.mean(list_over_snr_fAD_bundle1[it_snr][it_encoding])
                mean_fFA = np.mean(list_over_snr_fFA_bundle1[it_snr][it_encoding])
                mean_fRD = np.mean(list_over_snr_fRD_bundle1[it_snr][it_encoding])
                mean_deviation = np.mean(list_over_snr_deviation_bundle1[it_snr][it_encoding])
                weights_fAD = np.ones_like(list_over_snr_fAD_bundle1[it_snr][it_encoding])/float(len(list_over_snr_fAD_bundle1[it_snr][it_encoding]))
                weights_fFA = np.ones_like(list_over_snr_fFA_bundle1[it_snr][it_encoding])/float(len(list_over_snr_fFA_bundle1[it_snr][it_encoding]))
                weights_fRD = np.ones_like(list_over_snr_fRD_bundle1[it_snr][it_encoding])/float(len(list_over_snr_fRD_bundle1[it_snr][it_encoding]))
                weights_deviation = np.ones_like(list_over_snr_deviation_bundle1[it_snr][it_encoding])/float(len(list_over_snr_deviation_bundle1[it_snr][it_encoding]))

                ax0.hist(list_over_snr_fAD_bundle1[it_snr][it_encoding], weights=weights_fAD, bins=number_bins, range=(0, 0.003), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax1.hist(list_over_snr_fFA_bundle1[it_snr][it_encoding], weights=weights_fFA, bins=number_bins, range=(0, 1), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9, label=snr_label_list[it_snr])
                ax2.hist(list_over_snr_fRD_bundle1[it_snr][it_encoding], weights=weights_fRD, bins=number_bins, range=(0, 0.00075), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax3.hist(list_over_snr_deviation_bundle1[it_snr][it_encoding], weights=weights_deviation, bins=number_bins, range=(0, 0.4), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax0.axvline(x=mean_fAD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax1.axvline(x=mean_fFA, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax2.axvline(x=mean_fRD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax3.axvline(x=mean_deviation, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)

                ax0.axvline(x=D_para_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax1.axvline(x=FA_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax2.axvline(x=D_perp_fiber, color="Black", linestyle='-', linewidth=l_w)

            for tick in ax0.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax0.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            ax0.set_xticks([0, 0.001, 0.002, 0.003])
            ax0.set_xticklabels([r"0", r"1", r"2", r"3"])
            ax1.set_xticks([0, 0.25, 0.5, 0.75, 1])
            ax1.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75", r"1"])
            ax2.set_xticks([0, 0.00025, 0.0005, 0.00075])
            ax2.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75"])

            ax0.set_xlim(0, 0.003)
            ax1.set_xlim(0, 1)
            ax2.set_xlim(0, 0.00075)
            ax3.set_xlim(0, 0.4)

            ax0.set_ylim(0, 0.45)
            ax1.set_ylim(0, 0.5)
            ax2.set_ylim(0, 0.3)
            ax3.set_ylim(0, 1)

            ax1.legend(loc='upper left', prop={'size': legend_size}, ncol=1)

            plt.tight_layout()
            plt.savefig(comparison_directory + "histograms_bundle1_Diso_" + Diso_string + "_" + encoding + ".pdf")
            plt.close()


            fig, axes = plt.subplots(nrows=2, ncols=2)
            ax0, ax1, ax2, ax3 = axes.flatten()
            plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=0.3)

            ax0.set_title(r'fAD', fontsize=title_size)
            ax1.set_title(r'fFA', fontsize=title_size)
            ax2.set_title(r'fRD', fontsize=title_size)
            ax3.set_title(r'$\Delta\Theta$', fontsize=title_size)

            for it_snr, snr_string in enumerate(snr_string_list):

                mean_fAD = np.mean(list_over_snr_fAD_bundle2[it_snr][it_encoding])
                mean_fFA = np.mean(list_over_snr_fFA_bundle2[it_snr][it_encoding])
                mean_fRD = np.mean(list_over_snr_fRD_bundle2[it_snr][it_encoding])
                mean_deviation = np.mean(list_over_snr_deviation_bundle2[it_snr][it_encoding])
                weights_fAD = np.ones_like(list_over_snr_fAD_bundle2[it_snr][it_encoding])/float(len(list_over_snr_fAD_bundle2[it_snr][it_encoding]))
                weights_fFA = np.ones_like(list_over_snr_fFA_bundle2[it_snr][it_encoding])/float(len(list_over_snr_fFA_bundle2[it_snr][it_encoding]))
                weights_fRD = np.ones_like(list_over_snr_fRD_bundle2[it_snr][it_encoding])/float(len(list_over_snr_fRD_bundle2[it_snr][it_encoding]))
                weights_deviation = np.ones_like(list_over_snr_deviation_bundle2[it_snr][it_encoding])/float(len(list_over_snr_deviation_bundle2[it_snr][it_encoding]))

                ax0.hist(list_over_snr_fAD_bundle2[it_snr][it_encoding], weights=weights_fAD, bins=number_bins, range=(0, 0.003), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax1.hist(list_over_snr_fFA_bundle2[it_snr][it_encoding], weights=weights_fFA, bins=number_bins, range=(0, 1), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9, label=snr_label_list[it_snr])
                ax2.hist(list_over_snr_fRD_bundle2[it_snr][it_encoding], weights=weights_fRD, bins=number_bins, range=(0, 0.00075), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax3.hist(list_over_snr_deviation_bundle2[it_snr][it_encoding], weights=weights_deviation, bins=number_bins, range=(0, 0.4), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax0.axvline(x=mean_fAD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax1.axvline(x=mean_fFA, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax2.axvline(x=mean_fRD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax3.axvline(x=mean_deviation, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)

                ax0.axvline(x=D_para_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax1.axvline(x=FA_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax2.axvline(x=D_perp_fiber, color="Black", linestyle='-', linewidth=l_w)

            for tick in ax0.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax0.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            ax0.set_xticks([0, 0.001, 0.002, 0.003])
            ax0.set_xticklabels([r"0", r"1", r"2", r"3"])
            ax1.set_xticks([0, 0.25, 0.5, 0.75, 1])
            ax1.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75", r"1"])
            ax2.set_xticks([0, 0.00025, 0.0005, 0.00075])
            ax2.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75"])

            ax0.set_xlim(0, 0.003)
            ax1.set_xlim(0, 1)
            ax2.set_xlim(0, 0.00075)
            ax3.set_xlim(0, 0.4)

            ax0.set_ylim(0, 0.45)
            ax1.set_ylim(0, 0.7)
            ax2.set_ylim(0, 0.6)
            ax3.set_ylim(0, 0.7)

            ax1.legend(loc='upper left', prop={'size': legend_size}, ncol=1)

            plt.tight_layout()
            plt.savefig(comparison_directory + "histograms_bundle2_Diso_" + Diso_string + "_" + encoding + ".pdf")
            plt.close()


            fig, axes = plt.subplots(nrows=2, ncols=2)
            ax0, ax1, ax2, ax3 = axes.flatten()
            plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=0.3)

            ax0.set_title(r'fAD', fontsize=title_size)
            ax1.set_title(r'fFA', fontsize=title_size)
            ax2.set_title(r'fRD', fontsize=title_size)
            ax3.set_title(r'$\Delta\Theta$', fontsize=title_size)

            for it_snr, snr_string in enumerate(snr_string_list):

                mean_fAD = np.mean(list_over_snr_fAD_bundle3[it_snr][it_encoding])
                mean_fFA = np.mean(list_over_snr_fFA_bundle3[it_snr][it_encoding])
                mean_fRD = np.mean(list_over_snr_fRD_bundle3[it_snr][it_encoding])
                mean_deviation = np.mean(list_over_snr_deviation_bundle2[it_snr][it_encoding])
                weights_fAD = np.ones_like(list_over_snr_fAD_bundle3[it_snr][it_encoding])/float(len(list_over_snr_fAD_bundle3[it_snr][it_encoding]))
                weights_fFA = np.ones_like(list_over_snr_fFA_bundle3[it_snr][it_encoding])/float(len(list_over_snr_fFA_bundle3[it_snr][it_encoding]))
                weights_fRD = np.ones_like(list_over_snr_fRD_bundle3[it_snr][it_encoding])/float(len(list_over_snr_fRD_bundle3[it_snr][it_encoding]))
                weights_deviation = np.ones_like(list_over_snr_deviation_bundle3[it_snr][it_encoding])/float(len(list_over_snr_deviation_bundle3[it_snr][it_encoding]))

                ax0.hist(list_over_snr_fAD_bundle3[it_snr][it_encoding], weights=weights_fAD, bins=number_bins, range=(0, 0.003), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax1.hist(list_over_snr_fFA_bundle3[it_snr][it_encoding], weights=weights_fFA, bins=number_bins, range=(0, 1), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9, label=snr_label_list[it_snr])
                ax2.hist(list_over_snr_fRD_bundle3[it_snr][it_encoding], weights=weights_fRD, bins=number_bins, range=(0, 0.00075), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax3.hist(list_over_snr_deviation_bundle3[it_snr][it_encoding], weights=weights_deviation, bins=number_bins, range=(0, 0.4), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax0.axvline(x=mean_fAD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax1.axvline(x=mean_fFA, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax2.axvline(x=mean_fRD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax3.axvline(x=mean_deviation, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)

                ax0.axvline(x=D_para_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax1.axvline(x=FA_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax2.axvline(x=D_perp_fiber, color="Black", linestyle='-', linewidth=l_w)


            for tick in ax0.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax0.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            ax0.set_xticks([0, 0.001, 0.002, 0.003])
            ax0.set_xticklabels([r"0", r"1", r"2", r"3"])
            ax1.set_xticks([0, 0.25, 0.5, 0.75, 1])
            ax1.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75", r"1"])
            ax2.set_xticks([0, 0.00025, 0.0005, 0.00075])
            ax2.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75"])

            ax0.set_xlim(0, 0.003)
            ax1.set_xlim(0, 1)
            ax2.set_xlim(0, 0.00075)
            ax3.set_xlim(0, 0.4)

            ax0.set_ylim(0, 0.3)
            ax1.set_ylim(0, 0.7)
            ax2.set_ylim(0, 0.6)
            ax3.set_ylim(0, 0.7)

            ax1.legend(loc='upper left', prop={'size': legend_size}, ncol=1)

            plt.tight_layout()
            plt.savefig(comparison_directory + "histograms_bundle3_Diso_" + Diso_string + "_" + encoding + ".pdf")
            plt.close()


            fig, axes = plt.subplots(nrows=2, ncols=2)
            ax0, ax1, ax2, ax3 = axes.flatten()
            plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=0.3)

            ax0.set_title(r'fAD', fontsize=title_size)
            ax1.set_title(r'fFA', fontsize=title_size)
            ax2.set_title(r'fRD', fontsize=title_size)
            ax3.set_title(r'$\Delta\Theta$', fontsize=title_size)

            for it_snr, snr_string in enumerate(snr_string_list):

                list_over_snr_fAD = np.array(list_over_snr_fAD_bundle1[it_snr][it_encoding]).tolist() + np.array(list_over_snr_fAD_bundle2[it_snr][it_encoding]).tolist() + np.array(list_over_snr_fAD_bundle3[it_snr][it_encoding]).tolist()
                list_over_snr_fFA = np.array(list_over_snr_fFA_bundle1[it_snr][it_encoding]).tolist() + np.array(list_over_snr_fFA_bundle2[it_snr][it_encoding]).tolist() + np.array(list_over_snr_fFA_bundle3[it_snr][it_encoding]).tolist()
                list_over_snr_fRD = np.array(list_over_snr_fRD_bundle1[it_snr][it_encoding]).tolist() + np.array(list_over_snr_fRD_bundle2[it_snr][it_encoding]).tolist() + np.array(list_over_snr_fRD_bundle3[it_snr][it_encoding]).tolist()
                list_over_snr_deviation = np.array(list_over_snr_deviation_bundle1[it_snr][it_encoding]).tolist() + np.array(list_over_snr_deviation_bundle2[it_snr][it_encoding]).tolist() + np.array(list_over_snr_deviation_bundle3[it_snr][it_encoding]).tolist()

                mean_fAD = np.mean(list_over_snr_fAD)
                mean_fFA = np.mean(list_over_snr_fFA)
                mean_fRD = np.mean(list_over_snr_fRD)
                mean_deviation = np.mean(list_over_snr_deviation)
                weights_fAD = np.ones_like(list_over_snr_fAD)/float(len(list_over_snr_fAD))
                weights_fFA = np.ones_like(list_over_snr_fFA)/float(len(list_over_snr_fFA))
                weights_fRD = np.ones_like(list_over_snr_fRD)/float(len(list_over_snr_fRD))
                weights_deviation = np.ones_like(list_over_snr_deviation)/float(len(list_over_snr_deviation))

                ax0.hist(list_over_snr_fAD, weights=weights_fAD, bins=number_bins, range=(0, 0.003), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax1.hist(list_over_snr_fFA, weights=weights_fFA, bins=number_bins, range=(0, 1), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9, label=snr_label_list[it_snr])
                ax2.hist(list_over_snr_fRD, weights=weights_fRD, bins=number_bins, range=(0, 0.00075), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax3.hist(list_over_snr_deviation, weights=weights_deviation, bins=number_bins, range=(0, 0.4), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9)
                ax0.axvline(x=mean_fAD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax1.axvline(x=mean_fFA, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax2.axvline(x=mean_fRD, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)
                ax3.axvline(x=mean_deviation, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)

                ax0.axvline(x=D_para_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax1.axvline(x=FA_fiber, color="Black", linestyle='-', linewidth=l_w)
                ax2.axvline(x=D_perp_fiber, color="Black", linestyle='-', linewidth=l_w)

            for tick in ax0.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax0.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            ax0.set_xticks([0, 0.001, 0.002, 0.003])
            ax0.set_xticklabels([r"0", r"1", r"2", r"3"])
            ax1.set_xticks([0, 0.25, 0.5, 0.75, 1])
            ax1.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75", r"1"])
            ax2.set_xticks([0, 0.00025, 0.0005, 0.00075])
            ax2.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75"])

            ax0.set_xlim(0, 0.003)
            ax1.set_xlim(0, 1)
            ax2.set_xlim(0, 0.00075)
            ax3.set_xlim(0, 0.4)

            ax0.set_ylim(0, 0.4)
            ax1.set_ylim(0, 0.5)
            ax2.set_ylim(0, 0.5)
            ax3.set_ylim(0, 0.45)

            ax1.legend(loc='upper left', prop={'size': legend_size}, ncol=1)

            plt.tight_layout()
            plt.savefig(comparison_directory + "histograms_all_bundles_Diso_" + Diso_string + "_" + encoding + ".pdf")
            plt.close()



            fig, axes = plt.subplots(nrows=1, ncols=2)
            ax0, ax1 = axes.flatten()
            plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=0.3)

            ax0.set_title(r'Fiberfox', fontsize=title_size)
            ax1.set_title(r'$f_{\mathrm{FW}}($Magic DIAMOND$)$', fontsize=title_size)

            fiberfox_fFW_to_plot = []
            fiberfox_fintrafiberwater_to_plot = []
            for it_x in range(nb_voxel_x):
                for it_y in range(nb_voxel_y):
                    for it_z in range(nb_voxel_z):
                        if fiberfox_fFW[it_x][it_y][it_z] != 1:
                            fiberfox_fFW_to_plot.append(fiberfox_fFW[it_x][it_y][it_z])
                            fiberfox_fintrafiberwater_to_plot.append(fiberfox_fintrafiberwater[it_x][it_y][it_z])

            mean_fFW = np.mean(fiberfox_fFW_to_plot)
            mean_fintrafiberwater = np.mean(fiberfox_fintrafiberwater_to_plot)
            ax0.hist(fiberfox_fFW_to_plot, bins=number_bins, range=(0, 1), facecolor="Black", alpha=alpha_hist, rwidth=0.9, label=r'$f_{\mathrm{FW}}$')
            ax0.hist(fiberfox_fintrafiberwater_to_plot, bins=number_bins, range=(0, 1), facecolor="DarkViolet", alpha=alpha_hist, rwidth=0.9, label=r'$f(D_{\mathrm{iso,extra}})$')
            ax0.axvline(x=(mean_fFW+mean_fintrafiberwater)/2., color="Black", linestyle='--', linewidth=l_w)

            for it_snr, snr_string in enumerate(snr_string_list):
                list_over_snr_fFW_to_plot = [x for x in list_over_snr_fFW[it_snr][it_encoding] if x != 1]
                mean_fFW = np.mean(list_over_snr_fFW_to_plot)
                ax1.hist(list_over_snr_fFW_to_plot, bins=number_bins, range=(0, 1), facecolor=color_snr_list[it_snr], alpha=alpha_hist, rwidth=0.9, label=snr_label_list[it_snr])
                ax1.axvline(x=mean_fFW, color=color_snr_list[it_snr], linestyle='--', linewidth=l_w)

            for tick in ax0.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax0.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_size)

            ax0.set_xticks([0, 0.25, 0.5, 0.75, 1])
            ax0.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75", r"1"])
            ax1.set_xticks([0, 0.25, 0.5, 0.75, 1])
            ax1.set_xticklabels([r"0", r"0.25", r"0.5", r"0.75", r"1"])

            ax0.set_xlim(0, 1)
            ax1.set_xlim(0, 1)

            ax0.set_ylim(0, 700)
            ax1.set_ylim(0, 700)

            ax0.legend(loc='upper right', prop={'size': legend_size}, ncol=1)
            ax1.legend(loc='upper right', prop={'size': legend_size}, ncol=1)

            plt.tight_layout()
            plt.savefig(comparison_directory + "histograms_fFW_Diso_" + Diso_string + "_" + encoding + ".pdf")
            plt.close()



def check_dir(DIR):
    if os.path.isdir(DIR):
        os.system("rm -r " + DIR)
        os.system("mkdir " + DIR)
    else:
        os.system("mkdir " + DIR)


def cartesian_to_spherical_direction(list_coord): # Projects onto the half-sphere of positive x
    x_coord = list_coord[0]
    y_coord = list_coord[1]
    z_coord = list_coord[2]

    r_coord = np.sqrt(x_coord*x_coord + y_coord*y_coord + z_coord*z_coord)

    if x_coord < 0:
        x_coord = -list_coord[0]
        y_coord = -list_coord[1]
        z_coord = -list_coord[2]

    theta_coord = np.arccos(z_coord/r_coord)

    if x_coord == 0 and y_coord == 0:
        phi_coord = 0
    elif x_coord == 0 and y_coord > 0:
        phi_coord = np.pi/2.
    elif x_coord == 0 and y_coord < 0:
        phi_coord = -np.pi/2.
    else:
        phi_coord = np.arctan(y_coord/x_coord)

    return [r_coord, theta_coord, phi_coord]


def spherical_to_cartesian_direction(list_coord):
    r_coord = list_coord[0]
    theta_coord = list_coord[1]
    phi_coord = list_coord[2]
    x_coord = r_coord*np.cos(phi_coord)*np.sin(theta_coord)
    y_coord = r_coord*np.sin(phi_coord)*np.sin(theta_coord)
    z_coord = r_coord*np.cos(theta_coord)

    return [x_coord, y_coord, z_coord]


def angular_difference(theta_1, theta_2, phi_1, phi_2):
    cosine_difference = np.cos(theta_1)*np.cos(theta_2) + np.sin(theta_1)*np.sin(theta_2)*np.cos(phi_1 - phi_2)
    if cosine_difference > 1:
        cosine_difference = 1
    elif cosine_difference < -1:
        cosine_difference = -1
    if np.abs(np.arccos(cosine_difference)) > np.pi/2.:
        return np.pi-np.arccos(cosine_difference)
    else:
        return np.arccos(cosine_difference)


def read_Nifti(nifti_file):
    return nib.load(nifti_file).get_data()


def get_affine(nifti_file):
    return nib.load(nifti_file).affine


def get_shape(nifti_file):
    return nib.load(nifti_file).shape


def create_Nifti(data, affine, path, string):
    img = nib.Nifti1Image(data, affine)
    img.to_filename(os.path.join(path, string + '.nii.gz'))


def reshape_and_Nifti(data, tuple_shape, affine, path, string):
    data_array = np.reshape(np.array(data), tuple_shape)
    create_Nifti(data_array, affine, path, string)


def interquartile_range_1(data_list): # Method 1 of https://en.wikipedia.org/wiki/Quartile
    sorted_list = sorted(data_list)
    N = len(data_list)
    if N%2 == 0:
        n_middle = int(N/2)
    else:
        n_middle = int((N-1)/2)
    list_low = sorted_list[0:n_middle]
    list_high = sorted_list[-n_middle:]
    if n_middle%2 == 0:
        Q1 = (list_low[int(n_middle/2)-1] + list_low[int(n_middle/2)])/2.
        Q3 = (list_high[int(n_middle/2)-1] + list_low[int(n_middle/2)])/2.
    else:
        Q1 = list_low[int((n_middle-1)/2)]
        Q3 = list_high[int((n_middle-1)/2)]
    IQR = Q3 - Q1
    return IQR


def interquartile_range_2(data_list): # Method 2 of https://en.wikipedia.org/wiki/Quartile
    sorted_list = sorted(data_list)
    N = len(data_list)
    if N%2 == 0:
        n_middle = int(N/2)
    else:
        n_middle = int((N-1)/2+1)
    list_low = sorted_list[0:n_middle]
    list_high = sorted_list[-n_middle:]
    if n_middle%2 == 0:
        Q1 = (list_low[int(n_middle/2)-1] + list_low[int(n_middle/2)])/2.
        Q3 = (list_high[int(n_middle/2)-1] + list_high[int(n_middle/2)])/2.
    else:
        Q1 = list_low[int((n_middle-1)/2)]
        Q3 = list_high[int((n_middle-1)/2)]
    IQR = Q3 - Q1
    return IQR


def interquartile_range_3(data_list): # Method 3 of https://en.wikipedia.org/wiki/Quartile, arithmetic mean of Methods 1 and 2
    sorted_list = sorted(data_list)
    N = len(data_list)
    if N%2 == 0:
        n_middle = int(N/2)
        list_low = sorted_list[0:n_middle]
        list_high = sorted_list[-n_middle:]
        if n_middle % 2 == 0:
            Q1 = (list_low[int(n_middle/2)-1] + list_low[int(n_middle/2)])/2.
            Q3 = (list_high[int(n_middle/2)-1] + list_high[int(n_middle/2)])/2.
        else:
            Q1 = list_low[int((n_middle-1) / 2)]
            Q3 = list_high[int((n_middle-1) / 2)]
    elif (N-1)%4 == 0:
        n_middle = int((N-1)/4)
        Q1 = 0.75*sorted_list[n_middle-1] + 0.25*sorted_list[n_middle]
        Q3 = 0.25*sorted_list[3*n_middle] + 0.75*sorted_list[3*n_middle+1]
    elif (N-3)%4 == 0:
        n_middle = int((N-3)/4)
        Q1 = 0.75*sorted_list[n_middle] + 0.25*sorted_list[n_middle+1]
        Q3 = 0.25*sorted_list[3*n_middle+1] + 0.75*sorted_list[3*n_middle+2]
    IQR = Q3 - Q1
    return IQR


def stack_metrics(file_list):
    data_tuple = tuple([read_Nifti(x) for x in file_list])
    data_stacked = np.stack(data_tuple, axis=-1)
    affine = get_affine(file_list[0])
    return data_stacked, affine

def get_mean_and_IQR(file_list, metrics_name, output_DIR):
    data_stacked, affine = stack_metrics(file_list)
    mean_data = np.mean(data_stacked, axis = -1)
    IQR_data = scipy.stats.iqr(data_stacked, axis = -1)
    create_Nifti(mean_data, affine, output_DIR, "mean_" + metrics_name)
    create_Nifti(IQR_data, affine, output_DIR, "IQR_" + metrics_name)


def indices_sort(list_):
    # Sort the list from largest to smallest value and return the corresponding list of indices
    return np.array(list_).argsort()[::-1]


def create_matrix(matrix_element):
    return [[matrix_element[0],matrix_element[1],matrix_element[2]],
            [matrix_element[1],matrix_element[3],matrix_element[4]],
            [matrix_element[2],matrix_element[4],matrix_element[5]]]


def average_matrix(matrix_list, fraction):
    if np.sum(fraction) == 0:
        return np.zeros((3,3))
    else:
        average = np.zeros((3,3))
        for it in range(len(matrix_list)) :
            average += fraction[it]*np.array(matrix_list[it])
        return 1./np.sum(fraction)*average


def FA(eigenvalues):
    if np.mean(eigenvalues) == 0:
        return 0
    else:
        V = np.std(eigenvalues)*np.std(eigenvalues)
        M = np.mean(eigenvalues)
        return np.sqrt(3./2)*np.sqrt(V/(M*M + V))


def microFA(MD, V_iso, V_aniso):
    if V_aniso == 0.:
        return 0
    else:
        return np.sqrt(3./2)*1/np.sqrt(1 + (MD*MD + V_iso)/(5./2*V_aniso))


def OP(FA, microFA):
    if FA == 0 or microFA == 0:
        return 0
    else:
        return np.clip(np.sqrt((3./(microFA*microFA) - 2.)/(3./(FA*FA) - 2.)), 0, 1)


def rgb_map(scalar, eigenvectors):
    return tuple([np.uint8(255*scalar*abs(x)) for x in eigenvectors])


def rgb_map_normalized_difference(normalized_list):
    N_x, N_y, N_z = normalized_list.shape
    map = []
    for it_x in range(N_x):
        for it_y in range(N_y):
            for it_z in range(N_z):
                if normalized_list[it_x][it_y][it_z] > 0:
                    map.append(tuple([np.uint8(255*normalized_list[it_x][it_y][it_z]),np.uint8(0),np.uint8(0)]))
                elif normalized_list[it_x][it_y][it_z] < 0:
                    map.append(tuple([np.uint8(0),np.uint8(0),np.uint8(255*abs(normalized_list[it_x][it_y][it_z]))]))
                else:
                    map.append(tuple([np.uint8(0),np.uint8(0),np.uint8(0)]))
    return map

def map_orientations(list_scalar, list_eigenvectors, nb_fascicles):
    list_weighted_main_eigenvectors = []
    for it in range(nb_fascicles):
        list_weighted_main_eigenvectors.extend([list_scalar[it]*list_eigenvectors[it][iter] for iter in range(3)])
    return tuple(np.array(list_weighted_main_eigenvectors))


def weighted_mean(list_, fraction):
    if np.sum(fraction) == 0:
        return 0
    else:
        return np.average(list_, weights=fraction)


def weighted_mean_list(list_, fraction):
    if np.sum(fraction) == 0:
        return [0, 0, 0]
    else:
        a = np.sum(fraction[it]*list_[it][0] for it in range(len(fraction)))/np.sum(fraction)
        b = np.sum(fraction[it]*list_[it][1] for it in range(len(fraction)))/np.sum(fraction)
        c = np.sum(fraction[it]*list_[it][2] for it in range(len(fraction)))/np.sum(fraction)
        return 1./(np.sqrt(a*a + b*b + c*c))*np.array([a, b, c])


def weighted_variance(list_, fraction):
    if np.sum(fraction) == 0 or 1. in fraction or 1 in fraction: # To exclude CSF
        return 0
    else:
        variance = [(list_[it] - weighted_mean(list_, fraction))*(list_[it] - weighted_mean(list_, fraction)) for it in range(len(list_))]
        return np.average(variance , weights=fraction)


if __name__ == '__main__':
    main()
