import numpy as np
import numpy.linalg as lng
import nibabel as nib

fx = nib.load("/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/vxmout/NEX_1_test_1bundles_angle0_sphjson/maps/bundle_x_binary_map.nii")
fy = nib.load("/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/vxmout/NEX_1_test_1bundles_angle0_sphjson/maps/bundle_y_binary_map.nii")
fy45 = nib.load("/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/vxmout/NEX_1_test_1bundles_angle0_sphjson/maps/bundle_y45_binary_map.nii")
base_dwi = nib.load("/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout/param_bdel1_nonoise/fiberfox.nii.gz")

out_image = np.zeros(base_dwi.get_shape()[:-1])
out_image_affine = base_dwi.get_affine()


def get_index_in_mask(index, mask_affine):
    return np.dot(lng.inv(mask_affine), np.dot(out_image_affine, index))


for index, x in np.ndenumerate(out_image):
    for mask in [fx, fy, fy45]:
        mask_index = get_index_in_mask(np.asarray(index + (1, )), mask.get_affine())
        mask_index = mask_index[:-1] / mask_index[-1]

        if any([a < 0 for a in mask_index]) or any([a >= b for a, b in zip(mask_index, mask.get_shape())]):
            continue
        print(index, mask_index)
        print((int(i) for i in tuple(mask_index)))
        out_image[index] += mask.get_data()[tuple(int(i) for i in tuple(mask_index))]

img = nib.Nifti1Image(out_image, out_image_affine)
nib.save(img, "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/vxmout/NEX_1_test_1bundles_angle0_sphjson/maps/gt_mosemap.nii")