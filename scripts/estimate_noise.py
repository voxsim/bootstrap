from toolbox.utils import dataset
from os.path import join
import nibabel as nib

from scipy.stats import rice, norm
from scipy.signal import convolve, windows
from numpy import linspace, ravel, array, sin, arctan, sqrt

from matplotlib import pyplot as plt

from utils.koay_inversion import compute_rice_parameters

base_root = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout"
param_ffmt = "param_{}_{}"
dwi_ffmt = "fiberfox.nii.gz"

diffusivities = ["d2-10", "d2-15", "d2-20", "d2-25"]
bdeltas = ["bdel0", "bdel1", "bdel-05"]

ground_truth = "nonoise"
noisy = ["snr5", "snr20", "snr50"]

pdf_dscr = 30

params_dict = {}

for diff in diffusivities:
    params_dict[diff] = {}
    for bdel in bdeltas:
        params_dict[diff][bdel] = []
        gt = nib.load(join(base_root, diff, param_ffmt.format(bdel, ground_truth), dwi_ffmt))
        #print("gt | MIN : {} | MAX : {} |".format(gt.get_data().min(), gt.get_data().max()))

        gt_unit = (gt.get_data() - gt.get_data().min()) / (gt.get_data().max() - gt.get_data().min())

        for nsy in noisy:
            no = nib.load(join(base_root, diff, param_ffmt.format(bdel, nsy), dwi_ffmt))
            noise = no.get_data() - gt.get_data()

            noise_unit = (noise - noise.min()) / (noise.max() - noise.min())

            nib.save(nib.Nifti1Image(noise, affine=no.affine), "{}/noise_{}_{}_{}.nii.gz".format(base_root, nsy, bdel, diff))

            #noise = (noise - noise.mean()) / sqrt(noise.var())
            grt = gt.get_data() # (gt.get_data() - gt.get_data().mean()) / sqrt(gt.get_data().var())

            params2 = norm.fit(noise)
            print(params2)

            pdf = (linspace(noise.min(), noise.max(), pdf_dscr), )

            pdf += (norm.pdf(pdf[0], *params2),)

            v_norm = sqrt(gt_unit.mean() ** 2. + noise_unit.mean() ** 2.)
            v = sqrt(gt.get_data().mean() ** 2. + noise.mean() ** 2.)

            params_dict[diff][bdel] += [(pdf, noise, no.get_data(), v, v_norm)]


for diff in params_dict.keys():
    fig1, axes1 = plt.subplots(len(noisy), len(params_dict[diff].keys()))
    fig1.suptitle("{}".format(diff))

    fig2, axes2 = plt.subplots(len(noisy), len(params_dict[diff].keys()))

    i = 0
    for bdel in params_dict[diff].keys():
        pkg = params_dict[diff][bdel]
        for j in range(len(noisy)):
            axes1[j][i].hist(ravel(pkg[j][1]), int(pkg[j][1].max() - pkg[j][1].min()) + 1, density=True)
            axes1[j][i].plot(pkg[j][0][0], pkg[j][0][-1], 'g-')
            axes1[j][i].set_title("bdelta : {}\n{:.4f} | {:.2f}".format(bdel, pkg[j][-1], pkg[j][-2]))

            axes2[j][i].hist(ravel(pkg[j][2]), int(pkg[j][2].max() - pkg[j][2].min()) + 1, density=True, stacked=True)

            pdf_space = linspace(rice.ppf(0.01, pkg[j][-1]), rice.ppf(0.99, pkg[j][-1]), 100)

            # axes2[j][i].plot((pdf_space - pdf_space.min()) * (pkg[j][2].max() - pkg[j][2].min()) / (pdf_space.max() - pdf_space.min()), rice.pdf(pdf_space, pkg[j][-1]))
        i += 1

    fig1.tight_layout()
    fig2.tight_layout()
    fig1.subplots_adjust(top=0.5)
    plt.show()