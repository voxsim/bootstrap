#! /usr/bin/env python

from __future__ import print_function

import sys
print(sys.path)

from toolbox.utils import LogFile, concatenate_multiple_md_data, dataset, process_queuer, execute_cli, ComputingQueue
from toolbox.external import diamond
from os import path, mkdir
from shutil import copyfile
import nibabel as nib
import nrrd
import numpy as np
from numpy.random.mtrand import randint

supercompute = True
benoit = False

base_root = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/md_bootstrap/"
dataset_root = path.join(base_root, "datasets")
subjects = ["Sujet5"]
linear_datasets = [
    "dMRI_linear_1_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_linear_2_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_linear_3_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_linear_4_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso"
]
spherical_datasets = [
    "dMRI_spherical_1_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_spherical_2_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso"
]
planar_datasets = [
    "dMRI_planar_1_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_planar_2_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_planar_3_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso",
    "dMRI_planar_4_mag_denoised_topup_eddy_bet_crop_upsampled_to_2mm_iso"
]

if supercompute:
    mose_map_root = path.join("datasets", "mosemap")
    if benoit:
        diamond_executable = "crlDCIEstimate"
    else:
        diamond_executable = "start_diamond"
else:
    mose_map_root = path.join(dataset_root, "mosemap")
    diamond_executable = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/dev/bld/mdiamond/bin/crlDCIEstimate"
mose_map_name = "Wholemose_mosemap.nrrd"

output_root = path.join(base_root, "output")
#output_mangled_datasets_root = path.join(output_root, "datasets")

log_file_name = "md_bootstrap.log"

nb_threads = 8
mangle_threads = 4
nb_diamond_threads = 24
nb_of_output_datasets = 100
slicing = np.index_exp[0:66,0:89,:]
outslicing = np.index_exp[:,:,:]
nb_of_directions = 90

log_file = LogFile(path.join(base_root, log_file_name), sys.stdout)

if supercompute:
    diamond_processor = ComputingQueue(path.join(base_root, "diamond_commands_cr5"))
else:
    diamond_processor = process_queuer(int(nb_threads - mangle_threads) / nb_diamond_threads)

def mangle_dataset(subject, datasets, dataset_number, subject_output_path, diamond_path, bbox):
    print("KKKK")
    sys.stdout.flush()
    shape = datasets["linlin"][0].shape
    outshape = shape
    dtype = datasets["linlin"][0].data.dtype
    print("HHHH")
    sys.stdout.flush()
    if supercompute:
        diamond_mangled_datasets_root = path.join("output", subject)
        diamond_output_root = path.join("output", subject, "diamond")
    else:
        diamond_mangled_datasets_root = subject_output_path
        diamond_output_root = diamond_path
    print("LLLL")
    sys.stdout.flush()
    diamond_handle = diamond.md_handle(diamond_executable, nb_diamond_threads)
    diamond_handle.append_argument("--mosemask {0}".format(path.join(mose_map_root, subject, mose_map_name)))
    diamond_handle.append_argument("--bbox {0}".format(",".join([str(bound) for bound in bbox])))
    diamond_handle.append_argument("--fractions_sumto1 0")
    print("DDDD")
    sys.stdout.flush()
    i = 0

    for dataset_type, dataset_list in datasets.iteritems():
        bvecs = [np.loadtxt(bvecs_file) for bvecs_file in [dataset_list[0].bvecs, dataset_list[1].bvecs]]
        print("AAAAA")
        sys.stdout.flush()
        mangled_data = np.ndarray(outshape, dtype)
        mangled_bvecs = np.ndarray(bvecs[0].shape, bvecs[0].dtype)
        print("BBBBB")
        sys.stdout.flush()
        try:
            for direction in range(shape[-1]):
                dataset_choice = randint(0, 2)
                mangled_data[outslicing + (direction,)] = dataset_list[dataset_choice].data[slicing + (direction,)]
                mangled_bvecs[..., direction] = bvecs[dataset_choice][..., direction]
        except Exception as e:
            log_file.write(e.message + "\n")
        print("CCCCC")
        sys.stdout.flush()
        mangled_dataset = dataset()
        mangled_dataset.affine = np.identity(4)
        mangled_dataset.header = dataset_list[0].header
        mangled_dataset.bvals = dataset_list[0].bvals
        mangled_dataset.shape = outshape
        mangled_dataset.data = mangled_data
        mangled_dataset.save(path.join(subject_output_path, "mangled_{0}_{1}".format(dataset_type, dataset_number)), False)
        np.savetxt(path.join(subject_output_path, "mangled_{0}_{1}.bvecs".format(dataset_type, dataset_number)), mangled_bvecs)

        copyfile(path.join(dataset_root, "multidimsum_{0}.md".format(dataset_type)),
                 path.join(subject_output_path, "mangled_{0}_{1}.md".format(dataset_type, dataset_number))
                  )

        acq_path = path.join(diamond_path, "mangled_{0}_{1}".format(dataset_type, dataset_number))
        if not path.exists(acq_path):
            mkdir(acq_path)

        index = [slice(bbox[j], bbox[j] + bbox[j + 3] - 1) for j in range(len(bbox))[0:3]]
        mangled_bboxed = mangled_dataset.data[index[0], index[1], index[2], ...]
        mangled_dataset.data = mangled_bboxed
        mangled_dataset.save(path.join(acq_path, "mangled_{0}_{1}_slices".format(dataset_type, dataset_number)), False, False)
        copyfile(path.join(subject_output_path, "mangled_{0}_{1}.bvecs".format(dataset_type, dataset_number)),
                path.join(acq_path, "mangled_{0}_{1}.bvecs".format(dataset_type, dataset_number))
                )

        log_file.write("Dastaset {0} mangled".format(i), False)
        i += 1

        diamond_command = diamond_handle.get_run_command(
            path.join(diamond_mangled_datasets_root, "mangled_{0}_{1}.nii".format(dataset_type, dataset_number)),
            path.join(
                acq_path if not supercompute else path.join(diamond_output_root,
                                                            "mangled_{0}_{1}".format(dataset_type, dataset_number)
                                                            ),
                "mangled_{0}_{1}.nrrd".format(dataset_type, dataset_number)
            ),
            verbose=False
        )

        acquisition_log = path.join(acq_path, "mangled_{0}_{1}.log".format(dataset_type, dataset_number))

        diamond_processor.queue_process(execute_cli,
                                        diamond_command,
                                        acq_path,
                                        diamond_path,
                                        "mangled_{0}_{1}".format(dataset_type, dataset_number),
                                        acquisition_log
                                        )

mangling_processor = process_queuer(mangle_threads - 1)

def start():

    def mangle_subject(subject, processor):
        subject_output_path = path.join(output_root, subject)
        if not path.exists(subject_output_path):
            mkdir(subject_output_path)

        tmp_path = path.join(subject_output_path, "tmp")
        if not path.exists(tmp_path):
            mkdir(tmp_path)

        diamond_path = path.join(subject_output_path, "diamond")
        if not path.exists(diamond_path):
            mkdir(diamond_path)

        concatenate_multiple_md_data(
             path.join(dataset_root, subject),
             [[linear_datasets[0], linear_datasets[1]], [linear_datasets[2], linear_datasets[3]],
              [linear_datasets[0], spherical_datasets[0]], [linear_datasets[2], spherical_datasets[1]],
              [linear_datasets[1], planar_datasets[0]], [linear_datasets[3], planar_datasets[2]],
              [planar_datasets[0], planar_datasets[1]], [planar_datasets[2], planar_datasets[3]],
              [planar_datasets[0], spherical_datasets[0]], [planar_datasets[2], spherical_datasets[1]]
              ],
             tmp_path + "/",
             ["linlin1", "linlin2", "linsph1", "linsph2", "linpla1", "linpla2", "plapla1", "plapla2", "plasph1", "plasph2"],
             False
        )

        lin_sph_concatenated = [path.join(tmp_path, dt) for dt in ["linsph1", "linsph2"]]
        lin_lin_concatenated = [path.join(tmp_path, dt) for dt in ["linlin1", "linlin2"]]
        lin_pla_concatenated = [path.join(tmp_path, dt) for dt in ["linpla1", "linpla2"]]
        pla_pla_concatenated = [path.join(tmp_path, dt) for dt in ["plapla1", "plapla2"]]
        pla_sph_concatenated = [path.join(tmp_path, dt) for dt in ["plasph1", "plasph2"]]

        datasets = {"linlin": [dataset(nib.load("{0}.nii.gz".format(dt)),
                                       "{0}.bval".format(dt),
                                       "{0}.bvec".format(dt)
                                       ) for dt in lin_lin_concatenated
                               ],
                    "linsph": [dataset(nib.load("{0}.nii.gz".format(dt)),
                                       "{0}.bval".format(dt),
                                       "{0}.bvec".format(dt)
                                       ) for dt in lin_sph_concatenated
                               ],
                    "linpla": [dataset(nib.load("{0}.nii.gz".format(dt)),
                                       "{0}.bval".format(dt),
                                       "{0}.bvec".format(dt)
                                       ) for dt in lin_pla_concatenated
                               ],
                    "plapla": [dataset(nib.load("{0}.nii.gz".format(dt)),
                                       "{0}.bval".format(dt),
                                       "{0}.bvec".format(dt)
                                       ) for dt in pla_pla_concatenated
                               ],
                    "plasph": [dataset(nib.load("{0}.nii.gz".format(dt)),
                                       "{0}.bval".format(dt),
                                       "{0}.bvec".format(dt)
                                       ) for dt in pla_sph_concatenated
                               ]
                    }

        shape = datasets["linlin"][0].shape

        bbox = [0, 47, 0, shape[0], 3, shape[2]]

        for i in range(nb_of_output_datasets):
            processor.queue_process(mangle_dataset, subject, datasets, i, subject_output_path, diamond_path, bbox)

    for subject in subjects:
        mangle_subject(subject, mangling_processor)

    mangling_processor.join()
    diamond_processor.join()


if __name__ == "__main__":
    start()
