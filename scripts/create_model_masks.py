from __future__ import print_function

from os.path import join
from numpy import zeros_like, ones_like, concatenate as cat, sum, newaxis
import nibabel as nib
import nrrd

data_path = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout"
foldername_fmt = "param_{}_{}"
data_noises = ["nonoise"]
data_bdels = ["bdel1"]

model_mask_base = "compartment{}.nrrd"
model_mask_base2 = "dcompartment{}.nrrd"

from_scratch = False

if from_scratch:
    for noise in data_noises:
        for bdel in data_bdels:
            base_mask, hd = nrrd.read(join(data_path, model_mask_base.format(2)))

            extra_axonal_2, hd2 = nrrd.read(join(data_path,  model_mask_base2.format(4)))
            extra_axonal_1 = zeros_like(extra_axonal_2)
            extra_axonal_1[(extra_axonal_2 <= 0) & (base_mask <= 0)] = ones_like(extra_axonal_2)[(extra_axonal_2 <= 0) & (base_mask <= 0)]
            extra_axonal_1[(base_mask <= 0.12) & ~(base_mask == 0)] = (1. - base_mask[(base_mask <= 0.12) & ~(base_mask == 0)] - 0.7)

            fiber_mask = ones_like(extra_axonal_2) - extra_axonal_2 - extra_axonal_1 - base_mask

            nrrd.write(data_path + "/dcompartment2.nrrd", base_mask, hd)
            nrrd.write(data_path + "/dcompartment3.nrrd", extra_axonal_1, hd)
            nrrd.write(data_path + "/dcompartment4.nrrd", extra_axonal_2, hd2)
            nrrd.write(data_path + "/dcompartment1.nrrd", fiber_mask, hd)

else:
    for noise in data_noises:
        for bdel in data_bdels:
            base_mask, hd = nrrd.read(join(data_path, model_mask_base2.format(2)))
            extra_axonal_1, hd1 = nrrd.read(join(data_path, model_mask_base2.format(3)))
            extra_axonal_2, hd2 = nrrd.read(join(data_path, model_mask_base2.format(4)))

            fiber_mask = ones_like(extra_axonal_2) - extra_axonal_2 - extra_axonal_1 - base_mask
            extra_axonal_1[fiber_mask < 0] += fiber_mask[fiber_mask < 0]
            fiber_mask[fiber_mask < 0] = 0

            nrrd.write(data_path + "/compartment1.nrrd", fiber_mask, hd)
            nrrd.write(data_path + "/compartment2.nrrd", base_mask, hd)
            nrrd.write(data_path + "/compartment3.nrrd", extra_axonal_1, hd1)
            nrrd.write(data_path + "/compartment4.nrrd", extra_axonal_2, hd2)

            sum_mask = cat((base_mask[..., newaxis], extra_axonal_1[..., newaxis], extra_axonal_2[..., newaxis], fiber_mask[..., newaxis]), axis=-1)
            print(cat((sum_mask[~(sum(sum_mask, axis=-1) == 1)], sum(sum_mask[~(sum(sum_mask, axis=-1) == 1)], axis=-1)[..., newaxis]), axis = -1))
