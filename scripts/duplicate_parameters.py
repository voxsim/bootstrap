from os.path import join
from shutil import copyfile

base_path = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/test"
base_naming = "param_bdel1_snr5"
files = [".ffp", ".ffp_VOLUME1.nrrd", ".ffp_VOLUME2.nrrd", ".ffp_VOLUME3.nrrd", ".ffp_VOLUME4.nrrd"]
parameters = ["10", "15", "20", "25"]


for parameter in parameters:
    for file in files:
        copyfile(
            join(base_path, "{}{}".format(base_naming, file)),
            join(base_path, "{}_d2-{}{}".format(base_naming, parameter, file))
        )
