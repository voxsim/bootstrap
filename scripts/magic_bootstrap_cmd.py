#!/usr/bin/env python

import argparse
import numpy as np
import nibabel as nib
import sys
from os import path, mkdir, getcwd
from multiprocessing import cpu_count, get_logger, log_to_stderr, Manager, Array
from random import randint
from shutil import copyfile

sys.path.append(getcwd())

from toolbox.external import diamond
from toolbox.utils import LogFile, dataset, execute_cli, concatenate_multiple_md_data, ComputingQueue, process_queuer, \
    CountingGetDict, flatten

DESCRIPTION = """
This script does bootstrapping on a variety of subjects and then submits the resulting
datasets to be reconstructed via Magic Diamond.

The script can be used to do the whole bootstrapping and magic diamond runs, or one or 
the other. It can also defer the execution of diamond (in case of a subsequent batch
submission on a cluster, per example) and write the commands to a file.
"""


acquisiton_dict = CountingGetDict()
acquisiton_dict["linlin"] = "linlin"
acquisiton_dict["linpla"] = "linpla"
acquisiton_dict["linsph"] = "linsph"
acquisiton_dict["plasph"] = "plasph"
acquisiton_dict["plapla"] = "plapla"

md_dict = {
    "linlin": lambda a: ["linear\n" for a in range(0, int(a / 2))] + ["linear\n" for a in range(0, int(a / 2))],
    "linpla": lambda a: ["linear\n" for a in range(0, int(a / 2))] + ["planar\n" for a in range(0, int(a / 2))],
    "linsph": lambda a: ["linear\n" for a in range(0, int(a / 2))] + ["spherical\n" for a in range(0, int(a / 2))],
    "plasph": lambda a: ["planar\n" for a in range(0, int(a / 2))] + ["spherical\n" for a in range(0, int(a / 2))],
    "plapla": lambda a: ["planar\n" for a in range(0, int(a / 2))] + ["planar\n" for a in range(0, int(a / 2))]
}


class DatasetsAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string):
        if not getattr(namespace, self.dest):
            setattr(namespace, self.dest, {
                values[0] : values[1:]
            })
        elif not getattr(namespace, self.dest).has_key(values[0]):
            getattr(namespace, self.dest)[values[0]] = values[1:]
        else:
            getattr(namespace, self.dest)[values[0]] += values[1:]


class PairingsAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string):
        if len(values) % 2 == 1:
            raise argparse.ArgumentError('Must supply a sequence of pairings with type and position')

        pairings = []

        for i in range(len(values)):
            if i % 2 == 1:
                continue
            if type(values[i]) not in [str]:
                raise argparse.ArgumentError('Invalid pairings passed')
            try:
                v = int(values[i + 1])
                pairings.append((values[i], v))
            except TypeError:
                raise argparse.ArgumentError('Invalid pairings passed')

        if not getattr(namespace, self.dest):
            setattr(namespace, self.dest, [
                pairings
            ])
        else:
            getattr(namespace, self.dest).append(pairings)



def buildArgParser():
    parser = argparse.ArgumentParser(
        prog='Magic Diamond Bootstrapper',
        description=DESCRIPTION
    )

    parser.add_argument('root', help="""Working directory for the command line. 
                                        This is where the initial data is and the outputs will be saved.
                                     """
                        )
    parser.add_argument('output', help="""Output directory name where all the generated data will be placed.
                                          If non-existent in the 'root' directory, the folder will be created.
                                       """
                        )

    bootstrapping = parser.add_argument_group('bootstrapping')
    magic_diamond = parser.add_argument_group('magic diamond')

    bootstrapping.add_argument('--datasets_root',
                               dest="datasets",
                               help="""Subdirectory containing the initial data.""",
                               default=''
                               )
    bootstrapping.add_argument('--subjects',
                               nargs='+',
                               dest="subjects",
                               help="""List of subjects names. Those must reflect the names of the directories
                                       containing the subjects initial data.
                                    """,
                               default=[]
                               )
    bootstrapping.add_argument('--acquisition',
                               nargs='+',
                               dest='acquisitions',
                               help="""Names of the datasets to be used in the bootstrapping
                                       operation, prefixed by the type of the dataset 
                                       (linear, planar, spherical). e.g. : linear dataset1
                                    """,
                               action=DatasetsAction
                               )
    bootstrapping.add_argument('--m_threads',
                               dest='mangling_p',
                               help="""Number of threads that can be used to create the bootstrapped datasets""",
                               type=int,
                               default=2
                               )
    bootstrapping.add_argument('-n',
                               dest='nb_output_datasets',
                               help="""Quantity of datasets to bootstrap from each subject.""",
                               type=int,
                               required=True
                               )
    bootstrapping.add_argument('--pairing',
                               nargs='+',
                               dest='pairings',
                               help="""Pairings to use for the creation of the bootstrapped data. 
                                       A sequence of the dataset type followed by its position in the list passed.
                                       e.g. : <linear 0 linear 1> or <linear 0 spherical 1> (without <>).
                                    """,
                               required=True,
                               action=PairingsAction
                               )

    magic_diamond.add_argument('--executable',
                               dest='diamond_exec',
                               help="""The diamond compilation to use for the reconstruction calls.""",
                               required=True
                               )
    magic_diamond.add_argument('--d_threads',
                               dest='diamond_p',
                               help="""Number of threads per diamond command called.""",
                               type=int,
                               default=1
                               )
    magic_diamond.add_argument('--mosemap',
                               dest='mosemap',
                               help="""Name of the mosemap file to use in the diamond commands.
                                       The map will be fetched in the datasets' root folder.
                                    """,
                               default=None
                               )
    magic_diamond.add_argument('--slicing',
                               nargs=3,
                               dest="slicing",
                               help="""Slicing to be applied to the dataset by diamond before computing the maps.
                                       Defined via 3 parameters : <Dimension, Center, Width> (e.g. : 0 23 5).
                                       The center can be defined as well by -1, in which case the center position 
                                       in the sets will be used.
                                    """,
                               type=int,
                               default=None
                               )
    magic_diamond.add_argument('--fractions_sumto1',
                               dest="norm_fractions",
                               help="""Diamond commands will sum all fractions to be normalized over fraction space.""",
                               action='store_true'
                               )
    magic_diamond.add_argument('--additionnal_args',
                               dest='diamond_args',
                               help="""Additionnal arguments to be passed to diamond calls.""",
                               default=None
                               )
    magic_diamond.add_argument('--to_txt',
                               dest="supercompute",
                               help="""Saves diamond commands to txt files instead of executing them.""",
                               action='store_true'
                               )

    return parser


def mangle_dataset(subject, dataset_number, datasets_path, subject_output_path, diamond_path, bbox, slicing, outslicing, datasets, args, log_file, diamond_processor):

    try:
        log_file.write("[{}] Starting mangling\n".format(dataset_number))

        shape = flatten(datasets.values())[0][0].shape
        outshape = shape
        dtype = flatten(datasets.values())[0][0].data.dtype

        log_file.write("[{}] Setting up diamond\n".format(dataset_number))

        if args.supercompute:
            diamond_mangled_datasets_root = path.join("output", subject)
            diamond_output_root = path.join("output", subject, "diamond")
        else:
            diamond_mangled_datasets_root = subject_output_path
            diamond_output_root = diamond_path

        diamond_handle = diamond.md_handle(args.diamond_exec, args.diamond_p)
        if args.mosemap:
            diamond_handle.append_argument("--mosemask {0}".format(path.join(datasets_path, args.mosemap)))
        else:
            diamond_handle.automose()
        diamond_handle.append_argument("--bbox {0}".format(",".join([str(bound) for bound in bbox])))
        if args.norm_fractions:
            diamond_handle.append_argument("--fractions_sumto1 0")
        diamond_handle.append_argument("--initstick 0.000518549,6.47343e-05,6.47343e-05")

        log_file.write("[{}] Mangling datasets\n".format(dataset_number))

        for dataset_type in datasets.keys():

            dataset_list = datasets[dataset_type]

            if len(dataset_list) > 0:
                log_file.write("[{}] {} : {}\n".format(dataset_number, dataset_type, dataset_list))

                for pair in dataset_list:
                    bvecs = [np.loadtxt(bvecs_file) for bvecs_file in [pair[0].bvecs, pair[1].bvecs]]

                    mangled_data = np.ndarray(outshape, dtype)
                    mangled_bvecs = np.ndarray(bvecs[0].shape, bvecs[0].dtype)

                    log_file.write("[{}] {} : Loaded data\n".format(dataset_number, dataset_type))

                    for direction in range(shape[-1]):
                        dataset_choice = randint(0, len(pair) - 1)
                        mangled_data[outslicing + (direction,)] = pair[dataset_choice].data[slicing + (direction,)]
                        mangled_bvecs[..., direction] = bvecs[dataset_choice][..., direction]

                    mangled_dataset = dataset()
                    mangled_dataset.affine = np.identity(4)
                    mangled_dataset.header = pair[0].header
                    mangled_dataset.bvals = pair[0].bvals
                    mangled_dataset.shape = outshape
                    mangled_dataset.data = mangled_data

                    log_file.write("[{}] {} : Saving dataset\n".format(dataset_number, dataset_type))

                    mangled_dataset.save(path.join(subject_output_path, "mangled_{0}_{1}".format(dataset_type, dataset_number)), False)
                    np.savetxt(path.join(subject_output_path, "mangled_{0}_{1}.bvecs".format(dataset_type, dataset_number)), mangled_bvecs)

                    with open(path.join(subject_output_path, "mangled_{0}_{1}.md".format(dataset_type, dataset_number)), "w+") as f:
                        f.write("".join(md_dict[dataset_type](shape[-1])))

                    acq_path = path.join(diamond_path, "mangled_{0}_{1}".format(dataset_type, dataset_number))
                    if not path.exists(acq_path):
                        mkdir(acq_path)

                    log_file.write("[{}] {} : Saving bounded dataset\n".format(dataset_number, dataset_type))

                    index = [slice(bbox[j], bbox[j] + bbox[j + 3] - 1) for j in range(len(bbox))[0:3]]
                    mangled_bboxed = mangled_dataset.data[index[0], index[1], index[2], ...]
                    mangled_dataset.data = mangled_bboxed
                    mangled_dataset.save(path.join(acq_path, "mangled_{0}_{1}_slices".format(dataset_type, dataset_number)), False, False)
                    copyfile(path.join(subject_output_path, "mangled_{0}_{1}.bvecs".format(dataset_type, dataset_number)),
                             path.join(acq_path, "mangled_{0}_{1}_slices.bvecs".format(dataset_type, dataset_number))
                             )
                    copyfile(path.join(subject_output_path, "mangled_{0}_{1}.bvals".format(dataset_type, dataset_number)),
                             path.join(acq_path, "mangled_{0}_{1}_slices.bvals".format(dataset_type, dataset_number))
                             )

                    diamond_command = diamond_handle.get_run_command(
                        path.join(diamond_mangled_datasets_root, "mangled_{0}_{1}.nii".format(dataset_type, dataset_number)),
                        path.join(
                            acq_path if not args.supercompute else path.join(diamond_output_root,
                                                                        "mangled_{0}_{1}".format(dataset_type, dataset_number)
                                                                        ),
                            "mangled_{0}_{1}.nrrd".format(dataset_type, dataset_number)
                        ),
                        verbose=False
                    )

                    log_file.write("[{}] {} : Diamond command finalized\n".format(dataset_number, dataset_type))

                    acquisition_log = path.join(acq_path, "mangled_{0}_{1}.log".format(dataset_type, dataset_number))

                    log_file.write("[{}] {} : Running diamond processor\n".format(dataset_number, dataset_type))

                    diamond_processor.queue_process(execute_cli,
                                                    diamond_command,
                                                    acq_path,
                                                    diamond_path,
                                                    "mangled_{0}_{1}".format(dataset_type, dataset_number),
                                                    acquisition_log
                                                    )
    except:
        get_logger().error(sys.exc_info()[0])
        log_file.write("[{}] - Error while processing the dataset\n".format(dataset_number))
        log_file.write(sys.exc_info()[0] + "\n")


def mangle_subject(subject, processor, dataset_root, slicing, outslicing, diamond_processor, args, log_file):

    log_file.write("[M] Subject {}\n".format(subject))

    subject_output_path = path.join(args.root, args.output, subject)
    if not path.exists(subject_output_path):
        mkdir(subject_output_path)

    tmp_path = path.join(subject_output_path, "tmp")
    if not path.exists(tmp_path):
        mkdir(tmp_path)

    diamond_path = path.join(subject_output_path, "diamond")
    if not path.exists(diamond_path):
        mkdir(diamond_path)

    print("[M] {} - Pairings {}".format(subject, args.pairings))

    concat_pairings = [
        "{}{}".format(*acquisiton_dict["{}{}".format(pairing[0][0][:3], pairing[1][0][:3])]) for pairing in args.pairings
    ]

    print("[M] {} - Concatenating datasets".format(subject))

    concatenate_multiple_md_data(
        path.join(dataset_root, subject),
        [[args.acquisitions[data[0]][data[1]] for data in pairing] for pairing in args.pairings],
        tmp_path + "/",
        concat_pairings,
        False
    )

    datasets = {k: Array(dataset, dataset(), lock=False) for k in acquisiton_dict.keys()}

    for pairing in concat_pairings:
        dt = path.join(tmp_path, pairing)
        datasets[pairing[:-1]].append(dataset(nib.load("{0}.nii.gz".format(dt)),
                                                       "{0}.bval".format(dt),
                                                       "{0}.bvec".format(dt)
                                              )
                                      )

    for k, v in datasets.iteritems():
        datasets[k] = zip(v[0::2], v[1::2])

    shape = flatten(datasets.values())[0][0].shape

    bbox = [0, int(shape[1] / 2 - 1), 0, shape[0], 3, shape[2]]

    print("[M] {} - Mangling subject".format(subject))
    res = []
    m = Manager()
    m.connect()
    print(datasets)
    d = m.dict()
    for k, dt in datasets.iteritems():
        d[k] = m.list(dt)
    ns = m.Namespace()
    ns.mosemap = args.mosemap if args.diamond_exec else None
    ns.diamond_exec = args.diamond_exec if args.diamond_exec else None
    ns.diamond_p = args.diamond_p if args.norm_fractions else None
    ns.norm_fractions = args.norm_fractions if args.norm_fractions else None
    ns.supercompute = args.supercompute if args.supercompute else None

    for i in range(args.nb_output_datasets):
        res.append(processor.queue_process(mangle_dataset,
                                subject,
                                i,
                                dataset_root,
                                subject_output_path,
                                diamond_path,
                                bbox,
                                slicing,
                                outslicing,
                                d,
                                ns,
                                log_file,
                                diamond_processor
                                ))

    for r in res:
        r.wait()
        print(r.successful())
        print(r.get())


def main():
    log_to_stderr()

    parser = buildArgParser()
    args = parser.parse_args()

    supercompute = args.supercompute

    base_root = args.root
    dataset_root = path.join(base_root, args.datasets)
    subjects = args.subjects

    log_file_name = "md_bootstrap.log"
    log_file = LogFile(path.join(base_root, log_file_name), sys.stdout)

    nb_threads = cpu_count()
    mangle_threads = args.mangling_p
    nb_diamond_threads = args.diamond_p

    slicing = np.index_exp[...]
    outslicing = np.index_exp[...]

    if supercompute:
        diamond_processor = ComputingQueue(path.join(base_root, "diamond_commands"))
    else:
        diamond_processor = process_queuer(int(nb_threads - mangle_threads) / nb_diamond_threads)

    mangling_processor = process_queuer(mangle_threads - 1)

    for subject in subjects:
        mangle_subject(subject, mangling_processor, dataset_root, slicing, outslicing, diamond_processor, args, log_file)

    mangling_processor.join()
    diamond_processor.join()
    log_file.close()


if __name__ == "__main__":
    main()