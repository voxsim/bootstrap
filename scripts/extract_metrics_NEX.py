from __future__ import print_function

import os
from math import *
import numpy as np
import numpy.linalg as linalg
import nibabel as nib
import nrrd #from pynrrd
import itertools
from tqdm import tqdm
# import time
# import datetime
# start_time = datetime.datetime.now()
# print datetime.datetime.now() - start_time

WORKING_DIR = ""  # Directory where the outputs of the boostrapping code are located
SUBJECT = "Sujet4"  # The subject to process
bootstrap_root_name_DIR = "bootstrap"  # Same as the <output_bootstrap_root_name> of the bootstrapping
                                       # script, without the last '_', if there is one
data_type_list = ["linlin", "linsph"]  # Acquisition types in the bootstrap output of the subject
nb_DIAMOND_runs = 100  # Total number of datasets per pairing

is_asymmetric = False
data_format = ".nrrd" # Or .nii.gz or .nrrd
nb_fascicles = 3
D_CSF = 0.003 # CSF diffusivity

data_directory = os.path.join(WORKING_DIR, "results", SUBJECT, "diamond")

metrics_to_stat = ["fraction_FW", "max_fAD", "max_fRD", "max_fFA", "MD", "V_iso", "V_aniso", "MK_iso", "MK_aniso", "MK_tot", "microFA", "max_hei", "OP"]

DIR_output = data_directory + "AA_Statistics_on_metrics/"
DIR_output_type_list = [DIR_output + x + "/" for x in data_type_list]

if os.path.isdir(DIR_output):
    os.system("rm -r " + DIR_output)
    os.system("mkdir " + DIR_output)
    for x in DIR_output_type_list:
        os.system("mkdir " + x)
else:
    os.system("mkdir " + DIR_output)
    for x in DIR_output_type_list:
        os.system("mkdir " + x)


def main() :
    # DIR = data_directory + root_DIR + "_" + data_type_list[0] + "_0/"
    # root = root_DIR + "_" + data_type_list[0] + "_0"
    #
    # data_nrrd, options_nrrd = nrrd.read(DIR + root + "_b0.nrrd")
    # hei = nrrd.read(DIR + root + "_hei.nrrd")[0]
    #
    # # Extract number of compartments and voxel dimension lengths
    # nb_voxel_x, nb_voxel_y, nb_voxel_z = data_nrrd.shape[-3], data_nrrd.shape[-2], data_nrrd.shape[-1]
    #
    # # Create a Nifti affine in RAS+ convention, from the initial LPS convention
    #     # The minus signs project the left-posterior orientation onto the right-anterior one
    #     # options["space directions"] might be useful
    # origin = [float(options_nrrd["space origin"][x]) for x in range(3)]
    # # affine = np.eye(4)
    # # affine[0:3,0:3] = np.asarray(options["space directions"])
    # # affine[3,0:3] = origin
    # # print affine
    # affine = [[1, 0, 0, origin[0]],
    #           [0, 1, 0, -origin[1]],
    #           [0, 0, 1, origin[2]],
    #           [0, 0, 0, 1]]
    #
    # create_Nifti(data_nrrd, affine, DIR, "b0_0")



    extract_metrics(is_asymmetric, data_format, nb_fascicles, D_CSF)


def extract_metrics(is_asymmetric, data_format, nb_fascicles, D_CSF) :

    for data_type_index, data_type in enumerate(data_type_list):
        DIR_output_type = DIR_output_type_list[data_type_index]

        for run_number in range(nb_DIAMOND_runs):
            DIR = data_directory + bootstrap_root_name_DIR + "_" + data_type + "_" + str(run_number) + "/"
            root = bootstrap_root_name_DIR + "_" + data_type + "_" + str(run_number)

            print(root + ":")

            # Extract the nrrd fractions and heterogeneity indices
                # "options" is a dictionary containing relevant information
            fractions, options = nrrd.read(DIR + root + "_fractions.nrrd")
            hei = nrrd.read(DIR + root + "_hei.nrrd")[0]
            if is_asymmetric:
                hei_AD = nrrd.read(DIR + root + "_heiAD.nrrd")[0]

            # Extract number of compartments and voxel dimension lengths
            nb_compartments, nb_voxel_x, nb_voxel_y, nb_voxel_z = fractions.shape
            print("Data shape: (" + str(nb_voxel_x) + "," + str(nb_voxel_y) + "," + str(nb_voxel_z) + ")")
            print("Data space: " + str(options["space"]))  # Usually LPS

            # Create a Nifti affine in RAS+ convention, from the initial LPS convention
                # The minus signs project the left-posterior orientation onto the right-anterior one
                # options["space directions"] might be useful
            origin = [float(options["space origin"][x]) for x in range(3)]
            # affine = np.eye(4)
            # affine[0:3,0:3] = np.asarray(options["space directions"])
            # affine[3,0:3] = origin
            # print affine
            # affine = np.append(np.append(np.array(options["space directions"][1:]), [[origin[0]],[-origin[1]],[origin[2]]], axis=1), [[0,0,0,1]], axis=0)

            affine = [[1, 0, 0, origin[0]],
                        [0, 1, 0, -origin[1]],
                        [0, 0, 1, origin[2]],
                        [0, 0, 0, 1]]

            # Determine the relevant comparments and create direct Nifti files
            if nb_compartments == nb_fascicles + 1 :
                is_restricted_iso = False # In case of a restricted isotropic compartment
                file_name_fascicles = [DIR + root + "_t" + str(i) + ".nrrd" for i in range(nb_fascicles)]
                fractions_fascicles = np.array([fractions[0], fractions[1], fractions[2]])
                # Free water fraction
                create_Nifti(fractions[-1], affine, DIR_output_type, "fraction_FW_" + str(run_number))
                # Heterogeneity indices (AD means "axial")
                #create_Nifti(hei[0], affine, DIR_ouput_type, "hei_f0")
                #create_Nifti(hei[1], affine, DIR_ouput_type, "hei_f1")
                #create_Nifti(hei[2], affine, DIR_ouput_type, "hei_f2")
                #is is_asymmetric:
                    #create_Nifti(hei_AD[0], affine, DIR_ouput_type, "heiAD_f0")
                    #create_Nifti(hei_AD[1], affine, DIR_ouput_type, "heiAD_f1")
                    #create_Nifti(hei_AD[2], affine, DIR_ouput_type, "heiAD_f2")

            elif nb_compartments == nb_fascicles + 2 :
                is_restricted_iso = True
                # TO BE CODED LATER
                # file_name_fascicles = [DIR + root + "_t" + str(i) + ".nrrd" for i in range(nb_fascicles)]
                # fractions_fascicles = [fractions[0], fractions[1], fractions[2]]
                # create_Nifti(fractions[0], affine, DIR_ouput, "fraction_FW")
                # create_Nifti(fractions[1], affine, DIR_ouput, "fraction_restricted_iso")
                # create_Nifti(fractions[2] + fractions[3] + fractions[4], affine, DIR_ouput, "fraction_fascicles")

            # Extract the nrrd tensors for the different fascicles
                # data[matrix_element][coord_1][coord_2][coord_3]
                # data.shape[0] = 6 for fascicle 3x3 symmetric diffusion tensors
                # It is essential to limit the use of nrrd.read, which takes time
            data_fascicles = [nrrd.read(file_name_fascicles[nf])[0] for nf in range(nb_fascicles)]

            # Voxel averaged tensor
            data_dti = nrrd.read(DIR + root + "_dti.nrrd")[0]

            # Initialize metrics of interest
            list_MD = []
            list_microFA = []
            list_OP = []
            list_V_iso = []
            list_V_aniso = []
            list_MK_iso = []
            list_MK_aniso = []
            list_MK_tot = []
            list_max_fAD = []
            list_max_fRD = []
            list_max_fFA = []
            list_max_hei = []
            list_max_heiAD = []

            list_color_maxfFA = []
            list_color_fraction_and_maxfFA = []
            list_color_fraction_of_maxfFA = []
            list_color_mean_orientation = []

            list_orientations = np.zeros((nb_voxel_x,nb_voxel_y,nb_voxel_z,3*nb_fascicles), dtype = np.float32)

            list_MD_dti = []
            list_FA_dti = []
            list_colorFA_dti = []

            # Model-selection map
            # mosemap = nrrd.read(DIR + root + "_mosemap.nrrd")[0]
            # create_Nifti(mosemap, affine, DIR_output, "model_selection")


            for iter in tqdm(itertools.product(range(nb_voxel_x), range(nb_voxel_y), range(nb_voxel_z)), total = nb_voxel_x*nb_voxel_y*nb_voxel_z) :
                it_x = iter[0]
                it_y = iter[1]
                it_z = iter[2]

                if np.sum([fractions[i][it_x][it_y][it_z] for i in range(nb_compartments)]) == 0 :
                    list_MD.append(0)
                    list_microFA.append(0)
                    list_OP.append(0)
                    list_V_iso.append(0)
                    list_V_aniso.append(0)
                    list_MK_iso.append(0)
                    list_MK_aniso.append(0)
                    list_MK_tot.append(0)
                    list_max_fAD.append(0)
                    list_max_fRD.append(0)
                    list_max_fFA.append(0)
                    list_max_hei.append(0)
                    list_max_heiAD.append(0)
                    list_color_maxfFA.append(tuple([np.uint8(0) for x in range(3)]))
                    list_color_fraction_and_maxfFA.append(tuple([np.uint8(0) for x in range(3)]))
                    list_color_fraction_of_maxfFA.append(tuple([np.uint8(0) for x in range(3)]))
                    list_color_mean_orientation.append(tuple([np.uint8(0) for x in range(3)]))

                    list_orientations[it_x][it_y][it_z][:] = [0 for x in range(3*nb_fascicles)]

                    list_MD_dti.append(0)
                    list_FA_dti.append(0)
                    list_colorFA_dti.append(tuple([np.uint8(0) for x in range(3)]))

                    continue

                # How to find CSF voxels
                # if mosemap[it_x][it_y][it_z] == 0 and fractions[-1][it_x][it_y][it_z] != 0 :
                #     is_CSF = True
                # else :
                #     is_CSF = False

                # DTI metrics
                matrix_element_dti = [data_dti[n][it_x][it_y][it_z] for n in range(6)]

                # The eigenValues_dti[i] are in ascending order, eigenVectors_dti[:,i] are sorted accordingly
                eigenValues_dti, eigenVectors_dti = linalg.eigh(create_matrix(matrix_element_dti))

                list_MD_dti.append(np.mean(eigenValues_dti))
                list_FA_dti.append(FA(eigenValues_dti))

                list_colorFA_dti.append(rgb_map(list_FA_dti[-1], eigenVectors_dti[:,-1]))

                # list_eigenvalues = np.zeros(nb_fascicles, dtype=object)
                list_D_iso = []
                list_variance = []
                list_fAD = []
                list_fRD = []
                list_fFA = []

                main_eigenvectors_list = []
                fractions_fascicles_voxel = []
                matrix_list = []

                for nf in range(nb_fascicles):

                    data = data_fascicles[nf]
                    fractions_fascicles_voxel.append(fractions_fascicles[nf][it_x][it_y][it_z])

                    # Define the diffusion matrix for a given fascicle within the voxel (it_x,it_y,it_z)
                        # nrrd "3D-symmetric-matrix" convention: Dxx, Dxy, Dxz, Dyy, Dyz, Dzz
                    matrix_element = [data[n][it_x][it_y][it_z] for n in range(6)]
                    matrix = create_matrix(matrix_element)
                    matrix_list.append(matrix)

                    # The eigenValues[i] are in ascending order, eigenVectors[:,i] are sorted accordingly
                    eigenValues, eigenVectors = linalg.eigh(matrix)
                    main_eigenvectors_list.append(eigenVectors[:,-1])

                    # Specific intra-fascicle metrics
                    list_D_iso.append(np.mean(eigenValues))
                    list_fAD.append(eigenValues[-1])
                    list_fRD.append((eigenValues[0] + eigenValues[1])/2.)
                    list_variance.append(np.std(eigenValues)*np.std(eigenValues))
                    list_fFA.append(FA(eigenValues))

                average_eigenValues, average_eigenVectors = linalg.eigh(average_matrix(matrix_list, fractions_fascicles_voxel))

                # Lists of indices with metrics sorted from largest to smallest
                idx_fAD = indices_sort(list_fAD)
                idx_fRD = indices_sort(list_fRD)
                idx_fFA = indices_sort(list_fFA)

                list_max_fAD.append(np.array(list_fAD)[idx_fAD][0])
                list_max_fRD.append(np.array(list_fRD)[idx_fRD][0])
                list_max_fFA.append(np.array(list_fFA)[idx_fFA][0])

                orientations_list = map_orientations(list_fFA, main_eigenvectors_list, nb_fascicles)
                for it in range(3*nb_fascicles) :
                    list_orientations[it_x][it_y][it_z][it] = orientations_list[it]

                # Index of the fascicle with max_fFA
                it_max_fFA = list_fFA.index(np.max(list_fFA))
                list_color_maxfFA.append(rgb_map(list_max_fFA[-1], main_eigenvectors_list[it_max_fFA]))
                list_color_fraction_and_maxfFA.append(rgb_map(list_max_fFA[-1]*fractions_fascicles_voxel[it_max_fFA], main_eigenvectors_list[it_max_fFA]))
                list_color_fraction_of_maxfFA.append(rgb_map(fractions_fascicles_voxel[it_max_fFA], main_eigenvectors_list[it_max_fFA]))

                list_color_mean_orientation.append(rgb_map(FA(average_eigenValues), average_eigenVectors[-1]))

                list_V_aniso.append(2./5*weighted_mean(list_variance, fractions_fascicles_voxel))

                if is_restricted_iso :
                    continue # TO BE CODED LATER
                else :
                    list_D_iso.append(D_CSF)
                    fractions_voxel = np.array([fractions[i][it_x][it_y][it_z] for i in range(nb_compartments)])
                    list_MD.append(weighted_mean(list_D_iso, fractions_voxel))
                    list_V_iso.append(weighted_variance(list_D_iso, fractions_voxel))

                list_MK_iso.append(3*list_V_iso[-1]/(list_MD[-1]*list_MD[-1]))
                list_MK_aniso.append(3*list_V_aniso[-1]/(list_MD[-1]*list_MD[-1]))

                list_MK_tot.append(list_MK_iso[-1] + list_MK_aniso[-1])

                list_microFA.append(microFA(list_MD[-1], list_V_iso[-1], list_V_aniso[-1]))
                list_OP.append(OP(list_FA_dti[-1],list_microFA[-1]))

                list_max_hei.append(np.max(np.array([hei[i][it_x][it_y][it_z] for i in range(nb_fascicles)])))
                if is_asymmetric:
                    list_max_heiAD.append(np.max(np.array([hei_AD[i][it_x][it_y][it_z] for i in range(nb_fascicles)])))



            # Reshape the lists into arrays of dimension given by a tuple before "Niftification"
            tuple_voxel = (nb_voxel_x, nb_voxel_y, nb_voxel_z)
            tuple_color_voxel = (nb_voxel_x, nb_voxel_y, nb_voxel_z, 3)
            tuple_orientations_voxel = (nb_voxel_x, nb_voxel_y, nb_voxel_z, 3*nb_fascicles)

            reshape_and_Nifti(list_max_fAD, tuple_voxel, affine, DIR_output_type, "max_fAD_" + str(run_number))
            reshape_and_Nifti(list_max_fRD, tuple_voxel, affine, DIR_output_type, "max_fRD_" + str(run_number))
            reshape_and_Nifti(list_max_fFA, tuple_voxel, affine, DIR_output_type, "max_fFA_" + str(run_number))
            reshape_and_Nifti(list_MD, tuple_voxel, affine, DIR_output_type, "MD_" + str(run_number))
            reshape_and_Nifti(list_V_iso, tuple_voxel, affine, DIR_output_type, "V_iso_" + str(run_number))
            reshape_and_Nifti(list_V_aniso, tuple_voxel, affine, DIR_output_type, "V_aniso_" + str(run_number))
            reshape_and_Nifti(list_MK_iso, tuple_voxel, affine, DIR_output_type, "MK_iso_" + str(run_number))
            reshape_and_Nifti(list_MK_aniso, tuple_voxel, affine, DIR_output_type, "MK_aniso_" + str(run_number))
            reshape_and_Nifti(list_MK_tot, tuple_voxel, affine, DIR_output_type, "MK_tot_" + str(run_number))
            reshape_and_Nifti(list_microFA, tuple_voxel, affine, DIR_output_type, "microFA_" + str(run_number))
            reshape_and_Nifti(list_max_hei, tuple_voxel, affine, DIR_output_type, "max_hei_" + str(run_number))
            if is_asymmetric:
                reshape_and_Nifti(list_max_heiAD, tuple_voxel, affine, DIR_output_type, "max_heiAD_" + str(run_number))
            reshape_and_Nifti(list_OP, tuple_voxel, affine, DIR_output_type, "OP_" + str(run_number))
            reshape_and_Nifti(list_color_maxfFA, tuple_color_voxel, affine, DIR_output_type, "color_max_fFA_" + str(run_number))
            reshape_and_Nifti(list_color_fraction_and_maxfFA, tuple_color_voxel, affine, DIR_output_type, "color_fraction_and_max_fFA_" + str(run_number))
            reshape_and_Nifti(list_color_fraction_of_maxfFA, tuple_color_voxel, affine, DIR_output_type, "color_fraction_of_max_fFA_" + str(run_number))
            reshape_and_Nifti(list_color_mean_orientation, tuple_color_voxel, affine, DIR_output_type, "color_mean_orientations_" + str(run_number))

            create_Nifti(list_orientations, affine, DIR_output_type, "orientations_fFA_" + str(run_number))

            reshape_and_Nifti(list_MD_dti, tuple_voxel, affine, DIR_output_type, "MD_dti_" + str(run_number))
            reshape_and_Nifti(list_FA_dti, tuple_voxel, affine, DIR_output_type, "FA_dti_" + str(run_number))
            reshape_and_Nifti(list_colorFA_dti, tuple_color_voxel, affine, DIR_output_type, "colorFA_dti_" + str(run_number))

        # Compute statistics on certain metrics
        os.chdir(DIR_output_type)
        print("Computing statistics for " + data_type + " data...")
        for metrics_name in metrics_to_stat:
            get_mean_and_variance([metrics_name + "_" + str(run_number) + ".nii.gz" for run_number in range(nb_DIAMOND_runs)], metrics_name + "_" + data_type, DIR_output)


def read_Nifti(nifti_file):
    return nib.load(nifti_file).get_data()


def get_affine(nifti_file):
    return nib.load(nifti_file).affine


def get_shape(nifti_file):
    return nib.load(nifti_file).shape


def stack_metrics(file_list):
    data_tuple = tuple([read_Nifti(x) for x in file_list])
    data_stacked = np.stack(data_tuple, axis=-1)
    affine = get_affine(file_list[0])
    return data_stacked, affine


def get_mean_and_variance(file_list, metrics_name, output_DIR):
    data_stacked, affine = stack_metrics(file_list)
    mean_data = np.mean(data_stacked, axis = -1)
    var_data = np.var(data_stacked, axis = -1)
    create_Nifti(mean_data, affine, output_DIR, "mean_" + metrics_name)
    create_Nifti(var_data, affine, output_DIR, "var_" + metrics_name)

def create_Nifti(data, affine, path, string) :
    img = nib.Nifti1Image(data, affine)
    img.to_filename(os.path.join(path, string + '.nii.gz'))


def reshape_and_Nifti(data, tuple_shape, affine, path, string) :
    data_array = np.reshape(np.array(data), tuple_shape)
    create_Nifti(data_array, affine, path, string)


def indices_sort(list_) :
    # Sort the list from largest to smallest value and return the corresponding list of indices
    return np.array(list_).argsort()[::-1]


def create_matrix(matrix_element) :
    return [[matrix_element[0],matrix_element[1],matrix_element[2]],
            [matrix_element[1],matrix_element[3],matrix_element[4]],
            [matrix_element[2],matrix_element[4],matrix_element[5]]]


def average_matrix(matrix_list, fraction) :
    if np.sum(fraction) == 0 :
        return np.zeros((3,3))
    else :
        average = np.zeros((3,3))
        for it in range(len(matrix_list)) :
            average += fraction[it]*np.array(matrix_list[it])
        return 1./np.sum(fraction)*average


def FA(eigenvalues) :
    if np.mean(eigenvalues) == 0 :
        return 0
    else :
        V = np.std(eigenvalues)*np.std(eigenvalues)
        M = np.mean(eigenvalues)
        return sqrt(3./2)*sqrt(V/(M*M + V))


def microFA(MD, V_iso, V_aniso) :
    if V_aniso == 0. :
        return 0
    else :
        return sqrt(3./2)*1/sqrt(1 + (MD*MD + V_iso)/(5./2*V_aniso))


def OP(FA, microFA) :
    if FA == 0 or microFA == 0 :
        return 0
    else :
        return np.clip(sqrt((3./(microFA*microFA) - 2.)/(3./(FA*FA) - 2.)), 0, 1)


def rgb_map(scalar, eigenvectors) :
    return tuple([np.uint8(255*scalar*abs(x)) for x in eigenvectors])


def map_orientations(list_scalar, list_eigenvectors, nb_fascicles) :
    list_weighted_main_eigenvectors = []
    for it in range(nb_fascicles) :
        list_weighted_main_eigenvectors.extend([list_scalar[it]*list_eigenvectors[it][iter] for iter in range(3)])
    return tuple(np.array(list_weighted_main_eigenvectors))


def weighted_mean(list_, fraction) :
    if np.sum(fraction) == 0 :
        return 0
    else :
        return np.average(list_, weights = fraction)


def weighted_mean_list(list_, fraction) :
    if np.sum(fraction) == 0 :
        return [0, 0, 0]
    else :
        a = np.sum(fraction[it]*list_[it][0] for it in range(len(fraction)))/np.sum(fraction)
        b = np.sum(fraction[it]*list_[it][1] for it in range(len(fraction)))/np.sum(fraction)
        c = np.sum(fraction[it]*list_[it][2] for it in range(len(fraction)))/np.sum(fraction)
        return 1./(sqrt(a*a + b*b + c*c))*np.array([a, b, c])


def weighted_variance(list_, fraction) :
    if np.sum(fraction) == 0 or 1. in fraction or 1 in fraction : # To exclude CSF
        return 0
    else :
        variance = [(list_[it] - weighted_mean(list_, fraction))*(list_[it] - weighted_mean(list_, fraction)) for it in range(len(list_))]
        return np.average(variance , weights = fraction)


if __name__ == '__main__' :
    main()
