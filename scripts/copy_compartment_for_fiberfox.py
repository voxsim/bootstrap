from shutil import copyfile
from os.path import join

base_path = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout"
out_path = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo"

file_naming = "param_bdel{}_{}.ffp_VOLUME{}.nrrd"

bdels = ["0", "1", "-05"]
noises = ["nonoise", "snr5", "snr20", "snr50"]

base_naming = "compartment{}.nrrd"

for bdel in bdels:
    for noise in noises:

        for compartment in range(1, 5):

            copyfile(
                join(base_path, base_naming.format(compartment)),
                join(out_path, file_naming.format(bdel, noise, compartment))
            )
