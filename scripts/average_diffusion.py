from numpy import mean
import nibabel as nib

DWI_FILE = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout_huge/param_bdel1_nonoise/fiberfox"
MASK_FILE = "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/fiberfoxout_huge/param_bdel1_nonoise/mean_seg.nii"
EXT = ".nii.gz"


def average_diff():
    img = nib.load(DWI_FILE + EXT)
    mask = nib.load(MASK_FILE).get_data().astype("bool")

    img_mean = mean(img.get_data()[mask])

    nib.save(nib.Nifti1Image(img_mean, img.affine), DWI_FILE + "_mean" + EXT)


if __name__ == "__main__":
    average_diff()
