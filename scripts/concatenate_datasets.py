from toolbox.utils import concatenate_multiple_md_data
from os.path import join

base_root="/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/new_run/fiberfoxout/d2-30"
linear_root = "param_bdel1"
planar_root = "param_bdel-05"
spherical_root = "param_bdel0"

fiberfox_naming = "fiberfox"

snrs = ["nonoise"] # , "snr5", "snr20", "snr50"]


def concatenate():

    for snr in snrs:
        lroot = linear_root + "_" + snr
        proot = planar_root + "_" + snr
        sroot = spherical_root + "_" + snr
        concatenate_multiple_md_data(
            base_root,
            [
                [join(lroot, fiberfox_naming), join(lroot, fiberfox_naming)],
                [join(lroot, fiberfox_naming), join(proot, fiberfox_naming)],
                [join(proot, fiberfox_naming), join(proot, fiberfox_naming)],
                [join(lroot, fiberfox_naming), join(sroot, fiberfox_naming)]
            ],
            "/media/vala2004/b1f812ac-9843-4a1f-877a-f1f3bd303399/data/alexis/voxsim/bundles/solo/new_run/fiberfoxout/concatenated/d2-30/",
            ["{}_linlin_nn".format(snr), "{}_linpla_nn".format(snr), "{}_plapla_nn".format(snr),
             "{}_linsph_nn".format(snr)
            ]
        )


if __name__ == "__main__":
    concatenate()
